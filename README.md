# Dépot annexe au Gitlab MOOC NSI+SNT/CAPES

Ce dépot complémentaire contient essentiellement :

*  **les corrigés des exercices et supports
du MOOC NSI+SNT/CAPES, Les fondamentaux.**
* les bilans et retours du Mooc realisés régulièrement sur la base des réponses aux questionnaires et des données de la plateforme FUN

Rendez vous sur : https://gitlab.com/mooc-nsi-snt pour retrouver l'ensemble des contenus du MOOC.

En plus des exercices corrigés, le depot __support de cours a été recopié : il contient tous les documents autres que les fichiers .md des sequences du cours.
(code python ou diapos de cours) 

## Notes :

Epreuve_pratique_nsi.zip : David Roche : J'ai terminé la correction des 30 sujets de l'épreuve pratique de NSI. 
Vous pouvez les télécharger ici : https://pixees.fr/informatiquelycee/n_site/bac/epreuve_pratique_nsi.zip
 J'ai aussi ajouté un pdf avec des commentaires sur les exercices "problématiques". Retours et commentaires bienvenus !
