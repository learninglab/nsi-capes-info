# Bilan MOOC NSI 2 - 12 Janvier 23


##  Ouverture du MOOC le 7 février 2022

Bilan après 11 mois

**6141 inscrits en cours au 12 janvier 2023**

Fun/dashboard
  > * France	3232 apprenants soit    52.63%
  > * Maroc	508 apprenants soit    8.27%
  > * Haïti	190 apprenants soit    3.09%
  > * entre 3 et 2% : Côte d'Ivoire, Sénégal, Algérie

Questionnaire 1 - répartition 
   > * France (A66)	132	53,66%
 > * Maroc (A141)	20	8,13%
 > * Haïti (A85)	12	4,88%
 > * entre 4 et 3% : Côte d'Ivoire et Sénégal
 > * entre 3 et 2% : Tunisie, Cameroun, La Réunion, Togo, Bénin



<--! ### Sources données complètes :

* Profils : [NSI1-Q1-profils-0123.pdf](NSI1-Q1-profils-0123.pdf) / [NSI1-Q1-profils-0123.xls](NSI1-Q1-profils-0123.xls)
* Bilan, Avis :  [NSI1-Q2-bilan-0123.pdf](NSI1-Q2-bilan-0123.pdf) / [NSI1-Q2-bilan-0123.xls](NSI1-Q2-bilan-0123.xls)-->

### Attestations délivrées :

- Juin 22 : 44 attestations 3240 inscrits
- Septembre 22 : +9 attestations --> 53 attestions 4646 inscrits

à partir de janvier 2023 passage aux OpenBadges


### Questionnaire 1 : profil des participants = **246** réponses

* Hommes : 68,70%
* Femmes : 29,67%

### Ages des  participants

Categorie | Age | Décompte |	Pourcentage |
-------|-------|-------| -----------|
1 |Moins de 18 ans | 3 |  0,51% |
2 |De 18 à 24 ans  | 40 | 6,86%  | 
3 |De 25 à 35 ans  |  112 | 19,23% | 
4 |De 36 à 45 ans  |  116 | 19,9% |
5 |**De 46 à 55 ans**  | **187** | **32,06%** | 
6 |De 56 à 65 ans  | 90 | 15,44% |
7 |Plus de 65 ans  | 35 | 5,98% |  


### Niveau de formation 

* Master : 48,78%
* Licence : 22,76%
* Bac : 9,76%
* Doctorat : 8,54%

<--!![Niveau de formation des participants ](Niveau-de-formation-_NSI1-0123.png)-->


### Situation professionnelle

* Salariés en activité : 58,94%
* Étudiant : 13,01%
* En recherche d'emploi : 7,32%
* Autre : 14,23%
    * sur 35 personnes ayant répondu autre : 26 sont enseignants/profs/formateurs



![Situation pro ](Situationpro_NSI1_profil_0123)

#### Merci de nous préciser si :

* vous ne cherchez pas ici à enseigner mais à approfondir vos connaissances en informatique : 158   **27.10%** +
* vous êtes déjà enseignant·e du secondaire en NSI bien formé·e : 79  **13.55%** -
* vous êtes déjà enseignant·e du secondaire en SNT ou autre parcours informatique : 73  **12.52%** -
* vous êtes un·e professionnel·le de l'informatique : 66   **11.32%** +
* vous êtes enseignant·e du secondaire "non" informaticien·e : 57  **9.78%** +
* vous êtes étudiant·e en informatique : 49   **8.40%** +
* rien de tout cela : juste la curiosité vous amène ici : 45 **7.72%** +
* vous êtes parent et aimeriez mieux comprendre ce qui s'enseigne en NSI : 7   **1.20%** -
* vous étudiez en INSPÉ (Master MEEF) : 3   **0.51%** +

![pourquoi suivre le mooc](pourquoi-suivre-le-mooc-NSI1-0123.png)

### Pourquoi le Mooc vous intéresse ?

* Parce que le sujet m'intéresse particulièrement : 339  **58.15%** -
* Parce que selon moi il va m'aider dans la poursuite de ma carrière  : 337  **57.80%** +
* Parce que j'éprouve du plaisir et de la satisfaction à apprendre de nouvelles choses : 308.  **52.83%**
* Pour m'aider dans mon insertion professionnelle : 108 **18.52%**

#### Pour quelle(s) raison(s) vous êtes-vous inscrit(e) à ce MOOC ?

* vous souhaitez approfondir vos connaissances en informatique : 419   **71.87%** -
* vous aimeriez mieux comprendre ce qui s'enseigne en NSI : 185   **31.73%** -
* vous visez une reconversion : 129   **22.13%** +
* vous vous destinez à passer le CAPES d'informatique	: 62  **10.63%** +
* vous vous destinez à passer l'agrégation d'informatique : 52     **8.92%** +

### Prévision Temps consacré au Mooc 

* Heure par semaine :   
  * Moins d'1h : 29    4.97%
  * De 1 à 2h : 226   **38.77%** -
  * de 2 à 3h : 183   **31.39%** -
  * de 4 à 5h : 81   13.89% +
  * Plus de 5h : 64   10.98%
   
### Avez-vous des besoins spécifiques pour suivre ce MOOC (exemple : sous-titrage, version adaptée de document, etc.) ?
- Onglet "autre" : sur 6 réponses, 3 demandent le cours telechargeable au format pdf (fiches pédagogique) + 1 personne sur le forum

### Avez-vous d'autres commentaires, remarques ou suggestions d'amélioration à nous communiquer ?
- 3 réponses souhaitent des exemples applicables en classe (séquence péda), pour reussir les épreuves ecrites du CAPES
- format mobile demandé une fois pour suivre les vidéos

## Activité dans le Mooc

### QUIZ :

* Bloc 1 module 1 : 1er quiz : 917 réponses +467 (contre 450 en septembre 2022)
* Bloc 2 module 1 : 1er quiz : 137 réponses +91 (contre 46 en septembre 2022)
* Bloc 3 module 1 : 1er quiz : 158 réponses +112 (contre 46 en septembre 2022)
* Bloc 4 module 1 : 1er quiz : 179 réponses +136 (contre 43 en septembre 2022)

### Vidéos :

--> Analyse des premières vidéos de chaque Bloc ( chiffres relevés le 9 janv)

* Vidéo présentation Bloc 1 : 2667 vues +1615 (contre 1052 fois en septembre 2022)
* Vidéo présentation Bloc 2 : 149 vues
* Vidéo Bloc 3 - module 1 - chap 1 introduction 1 : 258 vues
* Vidéo présentation Bloc 4 : 265 vues

## Questionnaire de fin de MOOC : 26 réponses +10 (16 en septembre 2022)

### Le MOOC a-t-il satisfait vos attentes ?
Oui, comlèptement (A1) : 16  61.54%
Oui, plutôt (A2) : 10 38.46%

### L’effort à fournir était-il conforme à ce qui était annoncé ?
Oui, comlèptement (A1) : 11  42.31%
Oui, plutôt (A2) : 14 38.46% 53.85%

### Avez-vous des suggestions ou un avis concernant l’accessibilité du MOOC ?
- *tout à fait*
- *Aucune suggestions par rapport à la plateforme du MOOC. C'est conçu en bonne et d'une forme sans conteste*
- *J'aurais apprécié un sous titrage des vidéos.*
- *Son des vidéos assez inégal, parfois faible même avec son réglé au maximum, parfois fort. Dans l'idéal il faudrait essayer de l'uniformiser d'une vidéo à l'autre. Caractères petits sur certaines vidéos, c'est difficile à lire pour personnes pas très jeunes comme moi. Un débit de parole posé est agréable et permet de mieux comprendre*
- *Je veux que /nom/ Né à ... et habite à ... soient inscrits sur mon attestation*
- *Les questionnaires des quizz comportent parfois des questions ambigües*
- *Je ne suis entierement d'accord*
- *C'est claire pour moi*
- *oui*

### Avez-vous d'autres commentaires, remarques ou suggestions d'amélioration à nous communiquer ?
- *Merci*
- *utiliser Gmail*
- *Je trouve pertinent de proposer un pdf clair correspondant au contenu des vidéos. En effet, il est parfois plus facile de travailler sur un support écrit plutôt que sur une vidéo.*
- *Je remercie toutes les personnes qui ont contribué à ce MOOC de grande qualité, j'ai pris beaucoup de plaisir à le suivre*
- *Contenu très intéressant. Peut-être ajouter davantage de prospectives J'aimerais que les videos soient plus claire*
- *Je suis parfaitement d'avis*
- *no je vous remercie pour la formation*

### Quels sont, selon vous, les points positifs de ce cours ?
- *Ce MOOC explore le domaine de l'informatique dans une vision très large et global, et permet de donner des bases (voir plus) sur beaucoup de sujet*
- *Les professeurs tres bien, ils sont apte a dispenser leurs cours*
- *Les quizz, les exemples, les vidéos*
- *Tous les points mais je veux un cours qui se nomme Siences Informatiques*
- *pratique*
- *professeurs compétents qui savent communiquer leur passion, exemples concrets de mise en application*
- *Positif : Il permet d'aborder beaucoup de sujets. Négatif : Certains sont survolés de très haut !*
- *Il couvre tous les domaines du programme et donne des pistes pour approfondir.*
- *La présentation des cours en vidéo et en document texte*
- *utiliser des vidéo des quiz des tps*

### Avez-vous quelques suggestions et/ou points d'amélioration ? N'hésitez pas à préciser ce que vous auriez aimé voir dans ce cours qui n'y est pas, ce qui aurait pu être approfondi, ce qui vous a manqué ...
- *tout à bien fait*
- *Je suggère de produire des pdf reprenant les notions abordées dans les vidéos (à l'image du pdf sur les représentations numériques des nombres au début du mooc). Travailler sur un pdf me semble plus enrichissant que le support vidéo seul.*
- *Dans certaines vidéos, les explications commentant du code python ne doivent pas être faciles pour ceux qui ne connaissaient pas ce langage. Certaines questions dans les quiz sont ambigües ou pas claires, il faut souvent un premier essai pour vérifier notre compréhension de la question.*
- *Quelques quiz un peu "piégeux" (parfois plusieurs bonnes réponses à donner et aucun point alors qu'il manque juste une réponse, c'est un peu frustrant mais rien de grave) - a contrario d'autres sont très faciles... Parfois il y a juste les réponses et pas d'explication, j'aime mieux quand il y a des explications car on y apprend encore des détails intéressants. Aussi je n'ai pas trouvé la fiche sur les sémaphores que j'aurais bien aimé consulter.*
- *Technologies web plus approfondies$
- *La diversité des profs provoque un manque d'unicité entre les cours, probablement du au fait que personne n'a communiqué à personne le contenu et le niveau du contenu qu'il allait proposer.
Il manquait dans certains quizz la correction des réponses. Parfois certaines explications n'étaient pas très claires.
Selon les profs les quizz sont parfois très simple (sans réellement de piège) et d'autres sont presque impossibles car remplis de piège ou même pas vu en cours.*

### Avez-vous des retours à nous faire sur le forum ? N'hésitez pas à nous dire comment vous l'avez utilisé et ce que vous en avez pensé.
- *Je ne l'ai pas utilisé, je l'ai consulté une ou deux fois mais j'y ai trouvé peu d'intérêt (difficile de savoir de quoi va parler un post, et si c'est juste quelqu'un qui se plaint de n'avoir pas eu le point au quiz, bof)*
- *Réponse clair et très rapide de la part des intervenants donc que du positif !*
- *Le forum de ce mooc est incompréhensible. Il est ordonné ni par le temps ni par chapitre. Mes recherches de messages non lus me donnent bizarrement que un ou deux résultats !*
- *J'ai regretté de na pas avoir de réponses à des questions*

### Quelles sont les catégories thématiques (BLOC) que vous avez le mieux suivies/appronfondies ou qui vous ont été le plus utiles ? Commentez, par exemple, en citant les sujets que vous avez trouvés particulièrement intéressants ou moins intéressants.
BLOC 1 DATA 10 38.46%
BLOC 2 PROG 10 38.46%
BLOC 3 ALGO 8 30.77%
BLOC 4 ARS 10 38.46%

- *Surtout pour les bases de données que je ne connaissais pas du tout*
- *Manipulation de plusieurs entite en une seule valeur*
- *1.2 : Représentation des donnés : types construits / 1.3 : Traitements des données en tables*
- *programmation orienté objet*
- *pas de trop de maitrise de mon côté*
- *Surtout la programmation orientée objet que j'avais peu pratiquée*
- *Traitement des Donnees en tabe et la securite local*
- *2.1 : programmation orienté objet*
- *pas de trop de maitrise de mon côté*
- *le plus difficile*
- *+ utilisation des arbres*
- *Les cours de Jean-Marc Vincent sont toujours un régal*
- *pas accessible et pas de quizz*
- *Bien aimer et sa fait partir de ceux que je cherche de plus*
- *Ai bcp appris car c'est éloigné de mon domaine de départ*
- *+ processus, threads et communication par socket*
- *Particulièrement les processus et threads + les explications sur comment "mettre en ligne" un site web*
- *4.3 : réseaux / 4.4 : Technologies web*



# Focus BLOC 4 / Youtube

Publication des premières vidéos le **14 novembre 2022** (chiffres relevés le 10 janv)

* Nombre d'abonnés : 238 

### Commentaires :
11 au total :
* Remerciements : 6
* Concernant le support : 2  
   * *"Si c'est pour juste lire un texte ça sers à rien de faire des vidéos"*
   * *"Merci pour cette vidéo qui m'a aidé ! J'ai toutefois une petite critique à émettre. La personne dans la vidéo parle légèrement trop vite (certainement car elle lis un texte), ce qui rend la   compréhension légèrement plus compliquée."*
* Relevé d'erreurs / coquilles : 3
 
### Vidéos les plus visionnées :
1. [Module "Architecture materielle" ,présentation du cours](https://www.youtube.com/watch?v=ZXI7BGPJCHg&t=6s) : 459 vues
2. [Module "Architecture materielle" ,séquence "architecture minimale d'un système informatique" vidéo 1 ](https://www.youtube.com/watch?v=MKnRSMFx7hU&t=755s) : 429 vues
3. [Module "Architecture materielle" ,séquence "introduction à l'architecture des systemes infromatiques" vidéo 1 ](https://www.youtube.com/watch?v=bBJZ4jSs42A&t=48s) : 366 vues

### Incidence sur les inscriptions au MOOC
*14 novembre 2022 = Premières publications*
* 14 Novembre : 21 inscrits
* 15 Novembre : 65 inscrits  **+44**
* 16 Novembre : 42 inscrits
* 17 Novembre : 22 inscrits


