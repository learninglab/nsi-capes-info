# Bilan MOOC NSI1 - Mars 22


##  Ouverture du MOOC le 7 février 2022

**4505 inscrits en cours au 25 mars 22**
 
 > * 61, 27% France   -
 > *  4,31% Maroc
 > *  2.55%  Haïti (115)
 > * 2.51% Belgique 

### Sources données complètes :

* Profils : [NSI1-Q1-profils-0322.xls](NSI1-Q1-0322.xls) / [NSI1-Q1-profils-0322.pdf](NSI1-Q1-0322.pdf)
* Bilan, Avis :  [NSI1-Q2-bilan-0322.xls](NSI1-Q2-0322.xls) / [NSI1-Q2-bilan-0322.pdf](NSI1-Q2-0322.pdf)

### Questionnaire 1 profil des participants = 320 réponses

* Hommes : 73,12%
* Femmes : 26,25%


### Niveau de formation 

* Doctorat : 7%
* Master : 48%
* Licence : 24%
* DUT : 8%
* Bac : 8%
* ...

### Situation professionnelle

* Etudiant 7%
* Salariés : 63%
* ...

#### Question Mooc NSI1

* vous étudiez en INSPÉ (Master MEEF) 2 	0.62%
* vous êtes un·e professionnel·le de l'informatique  	32 	9.97%
* vous êtes parent et aimeriez mieux comprendre ce qui s'enseigne en NSI 	4 	1.25%
* vous êtes enseignant·e du secondaire "non" informaticien·e 	29 	9.03%
* vous êtes déjà enseignant·e du secondaire en NSI bien formé·e 	62 	19.31%
* vous êtes déjà enseignant·e du secondaire en SNT ou autre parcours informatique	44 	13.71%
* vous êtes étudiant·e en informatique (AO12) 6.85%
* rien de tout cela : juste la curiosité vous amène ici (AO13) 7.17%
* vous ne cherchez pas ici à enseigner mais à approfondir vos connaissances en informatique  78 	24.30% 

### Pourquoi le Mooc vous intéresse ?

* Utile pour ma carrière = 54% 
* Parce que le sujet m'intéresse particulièrement : 59 %

#### Question Mooc NSI1

* vous vous destinez à passer le CAPES d'informatique	24 	7.48%
* vous vous destinez à passer l'agrégation d'informatique 21 	6.54%
* vous souhaitez approfondir vos connaissances en informatique 235 	73.21%
* vous visez une reconversion (SQ004) 18.38%
* vous aimeriez mieux comprendre ce qui s'enseigne en NSI (SQ006) 32.71%
* Autre 6 4.06%

### Prévision Temps consacré au Mooc 

* Heure par semaine :   
  * De 1 à 2h : 42%  
  * de 2 à 3h : 34%  
  * de 4 à 5h : 11%  


## Activité dans le Mooc

### Vidéos :

**Bloc 1 module 1 : les premieres vidéos ont été vues autour de 1000 fois**

## la premiere video 1.1  a été vue 1052 fois


* Bloc 1 module 1 : 1er quiz : 450 réponses
* Bloc 2 module 1 : 1er quiz : 46 réponses
* Bloc 3 module 1 : 1er quiz : 46 réponses
* Bloc 4 module 1 : 1er quiz : 43 réponses 

