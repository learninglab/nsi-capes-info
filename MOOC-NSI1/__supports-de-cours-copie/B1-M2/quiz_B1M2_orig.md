1. Parmi les listes Python ci-dessous, laquelle est une liste en extension comportant 2 entiers, 1 chaîne de caractères et 3 booléens ?

    - `[-5, True, 10, True, False, 'mooc nsi']` (bonne réponse)
    - `[True] * 3 + [1, 2, 'hello']` 
    - `[0, 1, chaine, True, False, true]` 
    - `[True, '5', '8', False, False, hello]` 


2. Quelle(s) affirmation(s) ci-dessous sont _couramment_ admises lorsqu'on oppose _tableau_ (dans son acceptation algorithmique la plus stricte) des listes de Python ?

    - Le tableau est statique (bonne réponse)
    - Le tableau est non polymorphe (bonne réponse)
    - Le tableau se construit avec la fonction prédéfinie Python `Array`
    - Le tableau est non mutable


3. Quelle est la version en extension de la compréhension de liste suivante : `[e for a, n in [(1, 3), ('hello', 2)] for e in [a] * n]`

    - `[1, 1, 1, 'hello', 'hello']` (bonne réponse)
    - `[(1, 1, 1), ('hello', 'hello')]`
    - `[1, 'hello', 1, 'hello', 1]`
    - Aucune : la compréhension provoque une erreur

4. Quelle est la version en extension de la compréhension de liste suivante : `[[e for e in [a] * n] for a, n in [(1, 3), ('hello', 2)]]`

    - `[[1, 1, 1], ['hello', 'hello']]` (bonne réponse)
    - `[1, 1, 1, 'hello', 'hello']`
    - `[1, 'hello', 1, 'hello', 1]`
    - Aucune : la compréhension provoque une erreur

5. On considère la fonction suivante :

    ```python
    def foo(tab):
        i, j = 0, len(tab)-1
        while i < j:
            if tab[i] == 1:
                tab[i], tab[j] = tab[j], tab[i]
                j -= 1
            if tab[i] == 0:
                i += 1
    ```

    Et le tableau `tab = [0, 1, 1, 0, 0, 1, 0]`. Que vaut `tab` après l'appel `foo(tab)` ?

    - `[0, 0, 0, 0, 1, 1, 1]` (bonne réponse)
    - `[0, 1, 0, 1, 0, 1, 0]` 
    - `[1, 1, 1, 0, 0, 0, 0]` 
    - inchangé : la fonction provoque une erreur


6. Dans quelles situations algorithmiques ci-dessous l'utilisation d'un tuple s'avère judicieux ?

    - Une fonction qui doit renvoyer plusieurs valeurs (bonne réponse)
    - Une affectation multiple (bonne réponse)
    - La modélisation d'une série de valeurs numériques qui varient au cours de l'exécution du programme
    - Le paramètre d'une fonction de tri en place


7. Quelle instruction ci-dessous ne permet pas à la variable `d` de référencer un dictionnaire ?

    - `d = {e**2 for e in range(10)}`  (bonne réponse)
    - `d = dict([('lundi', 'monday'), ('mardi', 'tuesday'), ('mercredi', 'wednesday')])`
    - `d = {'4': {'00': 'Requête erronée', '01': 'Authentification requise'},'5': {'00': 'Erreur interne du serveur'}}`
    - `a = {'ville':'Paris'} ; d = dict(a, monument = 'Tour Eiffel')` 


8. On considère une collection _C_ d'environ 300000 chaînes de caractères. Option 1 : on stocke ces mots comme clés d'un dictionnaire `D` auxquelles on associe une certaine information. Option 2 : on stocke les mots dans une liste `L`. 

    De plus, une autre liste `L2` contient environ 100000 chaînes de caractères. Quelles manipulations sont plus efficaces avec le dictionnaire ?

    - Tester l'appartenance des mots de `L2` à la collection _C_ ? (bonne réponse)
    - Supprimer un mot donné de la collection (bonne réponse)
    - Ajouter un mot à la collection (bonne réponse)
    - Ordonner les mots de la collection







