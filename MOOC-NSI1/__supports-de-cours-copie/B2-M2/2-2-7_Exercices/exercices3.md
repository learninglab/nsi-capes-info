# Langages : exercice 3

[Exercice qui associe à chaque code, un nom de langage parmi la liste donnée]

Donnez le langage utilisé pour chaque code suivant :

Possibilités

- ALGOL68
- C
- COBOL
- C++
- FORTRAN
- JAVA
- Pascal
- Python


### réponse : Programme en ALGOL68


```algol68
COMMENT  Yves Roggeman - 2018/12
    This program counts how much data are needed to reach a given level
COMMENT

BEGIN
    INT level = 1024;
    INT sum := 0, nb := 0;
    WHILE
        on value error(stand in, (REF FILE file)BOOL: err);
        on file end(stand in, (REF FILE file)BOOL: err);
        INT data; read(data);
        IF data < 0 THEN GOTO err FI;
        sum +:= data;
        nb +:= 1;
        sum < level
    DO SKIP OD;
    write(nb)
    EXIT
err:
    print("Data error")
END
```

###  réponse : Programme en COBOL
```cobol
 IDENTIFICATION DIVISION.
 PROGRAM-ID.     COMPARE.
 AUTHOR.         YVES ROGGEMAN.
 DATE-WRITTEN.   2018/12.
 REMARKS.        THIS PROGRAM COUNTS HOW MUCH DATA ARE NEEDED
                 TO REACH A GIVEN LEVEL.
 ENVIRONMENT DIVISION.
 CONFIGURATION SECTION.
 SOURCE-COMPUTER. UBUNTU.
 OBJECT-COMPUTER. X64-86.
*    OpenCOBOL 2.2.0 - "cobc -std=cobol85 -fixed"
 INPUT-OUTPUT SECTION.
 FILE-CONTROL.
     SELECT INP ASSIGN TO "Compare_Data". 
     SELECT OUT ASSIGN TO DISPLAY.
 DATA DIVISION.
 FILE SECTION.
 FD  INP.
 01  FILLER      PICTURE X(80).
 FD  OUT.
 01  NB-OUT      PICTURE Z(4)9.
 WORKING-STORAGE SECTION.
 77  LEVEL       PICTURE 9999, USAGE IS COMPUTATIONAL
                             , VALUE IS 1024. 
 77  TOT         PICTURE 9(5), USAGE IS COMPUTATIONAL
                             , VALUE IS ZERO.
 77  DAT         PICTURE S9(5), USAGE IS COMPUTATIONAL.
 77  NB          PICTURE 9(5), USAGE IS COMPUTATIONAL
                             , VALUE IS ZERO.
 PROCEDURE DIVISION.
 MAIN.
     OPEN INPUT INP.
     PERFORM LOOP,
         WITH TEST AFTER, UNTIL TOT IS GREATER THAN LEVEL.
     CLOSE INP.
     OPEN OUTPUT OUT.
     WRITE NB-OUT FROM NB.
     CLOSE OUT.
     STOP RUN.
 LOOP.
     READ INP INTO DAT; AT END PERFORM ERR.
     IF DAT IS LESS THAN ZERO THEN PERFORM ERR.
     ADD DAT TO TOT.
     ADD 1 TO NB.
 ERR.
     CLOSE INP.
     DISPLAY "Data error".
     STOP RUN.
 
```


###  réponse : Programme en C++
```c++
/* Yves Roggeman - 2018/12 
 * This program counts how much data are needed to reach a given level
 */
#include <cstdlib> // abort()...
#include <iostream> // cin, cout...

int main () {
    const unsigned level = 1024;
    unsigned sum = 0, nb = 0;
    do {
        int data; std::cin >> data;
	if (!std::cin || data < 0)
            {std::cerr << "Data error" << std::endl; std::abort();}
	sum += data; ++nb;
    } while (sum < level);
    std::cout << nb << std::endl;
    return 0;
}
```

###  réponse : Programme en Python
```python
# -*- coding: utf-8 -*-
# Yves Roggeman - 2018/12 
# This program counts how much data are needed to reach a given level
#

import sys
level = 1024
sum=0
nb=0
while sum < level:
    try:
        data = int(input())
	if data < 0:
        raise Exception
    except Exception:
        print("Data error")
        sys.exit(-1)
    sum += data
    nb += 1
print(nb)
```

###  réponse : Programme en Java
```java
/* Yves Roggeman - 2018/12 
 * This program counts how much data are needed to reach a given level
 */
import java.util.Scanner; // nextInt...
public class Compare {
    public static void main (String[] args) {
        final int level = 1024;
	int sum = 0, nb = 0;
    do{
        int data = 0;
	try {
            data = (new Scanner(System.in)).nextInt();
            if (data < 0) throw new Exception();
	} catch(Exception err)
            {System.out.println("Data error"); System.exit(-1);}
        sum += data; ++nb;
     } while (sum < level);
     System.out.println(nb);
    }
}
```

###  réponse : programme en FORTRAN

```fortran
C Yves Roggeman - 2018/12 
C This program counts how much data are needed to reach a given level
C
      PROGRAM COMPARE
        IMPLICIT NONE
        INTEGER LEVEL, TOT, NB, DAT
        PARAMETER (LEVEL = 1024)
        TOT=0
        NB=0
        DO WHILE (TOT .LT. LEVEL)
          READ(*, *, ERR=999, END=999) DAT
          IF (DAT .LT. 0) GOTO 999
          TOT = TOT + DAT
          NB = NB + 1
        END DO
        PRINT*, NB
      STOP
999     PRINT*, 'Data error'
        STOP 1
      END
```

###  réponse : Programme en Pascal
```pascal
{ Yves Roggeman - 2018/12 
  This program counts how much data are needed to reach a given level
}
program compare;
    const level = 1024;
    var data, sum, nb: integer;
begin
    sum := 0; nb := 0;
    repeat
        read(data);
        if data < 0 then
            begin writeln('Data error'); halt(-1) end;
        sum := sum + data; nb := succ(nb)
    until sum >= level;
    writeln(nb)
end.
```

###  réponse : Programme en C
```c
/ * Yves Roggeman - 2018/12
  * This program counts how much data are needed to reach a given level
  */
#include <stdlib.h>
#include <stdio.h>

int main (void) {
    const unsigned level = 1024;
    unsigned sum = 0, nb = 0;
    do {
        int data, err = scanf("%d", &data);
	if (err <= 0 || data < 0)
            {fputs("Data error\n", stderr); abort();}
	sum += data; ++nb;
    } while (sum < level);
    printf("%d\n", nb);
    return 0;
}
```

