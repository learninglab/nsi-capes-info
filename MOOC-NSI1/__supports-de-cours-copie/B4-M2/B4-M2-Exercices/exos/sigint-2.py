#!/usr/bin/env python3

import os
import time
import signal

countSIGINT=0

def handler_sigint(sig,f):
    global countSIGINT
    countSIGINT=countSIGINT+1
    print("J'en suis à %d SIGINT\n"%countSIGINT)

def main():
    print('Je me lance et je suis', os.getpid())
    signal.signal(signal.SIGINT,handler_sigint)
    while True:
        signal.pause()
    os._exit(0)

main()

