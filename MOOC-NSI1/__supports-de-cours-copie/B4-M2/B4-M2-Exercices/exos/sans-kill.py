#!/usr/bin/env python3

import os
import time
import signal

def main():
    print('[père] mon pid est ', os.getpid())
    gpid = os.getpgid(os.getpid())
    newpid = os.fork()
    if newpid == 0:
        print('[fils] mon pid est ', os.getpid())
        for i in range(10):
            time.sleep(1)
            print('[fils] itération ', i)
    else:
        print('[père] je ne tue pas mon fils', newpid)
        time.sleep(3)
        
    print('j\'ai terminé', os.getpid())
    os._exit(0)

main()

