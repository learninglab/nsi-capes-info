

\chapter{Leçon 1 -- Introduction: Qu'est-ce qu'un ordinateur ? }\label{cha:intro}


\section{Définitions et étymologie}
L'objet de ce cours est, comme son titre l'indique, d'expliquer
comment \emph{fonctionne} un ordinateur. Le but ici est de dégager une
série de principes de base qui régissent et permettent d'expliquer le
fonctionnement des ordinateurs tels que nous les connaissons
aujourd'hui, mais aussi tels qu'ils ont toujours existé (et, espérons
le, tels qu'ils existeront dans le futur). Nous allons donc commencer
par nous demander ce qu'\emph{est} un ordinateur.

Commençons par observer que le mot <<~ordinateur~>> est un mot
relativement neuf dans l'acception qui nous intéresse ici. Il a été
proposé par Jacques \textsc{Perret}\footnote{Philologue français né le
  6 décembre 1906 et mort le 29 mars 1992, il est professeur à la
  Faculté de lettres de Paris (France).}, qui avait été consulté par
le directeur d'IBM France en 1955. IBM cherchait à l'époque un nom
français pour ces nouvelles machines que l'on nommait
\textit{computer} en anglais. Jacques \textsc{Perret} répond par une
lettre du 16 avril 1955 restée célèbre (voir \figurename~\ref{fig:lettre}).

\begin{figure}
  \centering
  \includegraphics[scale=.7]{images/Chap1/lettre-ordinateur.jpg}
  \caption[Lettre de J. \textsc{Perret}.]{\textit{Que diriez-vous
      d'ordinateur ?} La proposition de Jacques \textsc{Perret} pour nommer
    cette nouvelle machine\ldots}
  \label{fig:lettre}
\end{figure}
\medskip

En ce qui concerne maintenant le \emph{sens} du mot, Wikipedia
\cite{wiki:ordinateur} nous donne la définition suivante:
\begin{quote}
  Un ordinateur est un système de traitement de l'information
  \emph{programmable} [\ldots] et qui fonctionne par la lecture
  séquentielle d'un ensemble d'instructions, organisées en
  \emph{programmes}, qui lui font exécuter des \emph{opérations
    logiques et arithmétiques}.
\end{quote}
Le Petit Robert, pour sa part, nous donne la définition:
\begin{quote}
  Machine électronique de \emph{traitement numérique} de
  \emph{l'information}, exécutant à grande vitesse les instructions
  d'un \emph{programme enregistré}.
\end{quote}
Ces deux définitions mettent en avant plusieurs concepts essentiels:
\begin{enumerate}
\item L'ordinateur est une machine qui \emph{traite l'information}. Il
  reçoit de l'information et en produit, sur base de l'information
  reçue. Par exemple, un ordinateur peut être utilisé pour calculer
  les racines réelles d'une équation du second degré à une inconnue
  sur base des coefficients $a$, $b$ et $c$ de cette équation (c'est
  l'exemple que nous développons ci-dessous); ou pour élaborer la
  fiche de paye d'un travailleur sur base de ses prestations; ou pour
  afficher une vidéo sur base de données reçues \textit{via}
  Internet,\ldots
\item Pour traiter l'information, l'ordinateur \emph{exécute un
    programme}, qui est une séquence \emph{d'instructions}, indiquant
  chacune à traitement spécifique à exécuter. Cela signifie que
  l'ordinateur a été conçu de manière à être capable de réaliser
  certains traitements correspondants à des instructions pré-définies,
  mais qu'on peut utiliser ces instructions de base comme des briques
  pour construire des traitements plus complexes appelés
  \emph{programmes}.

  Par exemple, supposons que nous disposons d'un ordinateur capable
  d'exécuter les instructions \inasm{add}\ et \inasm{idiv}\ qui
  calculent respectivement la somme de deux nombres et la division
  (entière) d'un nombre par un autre. Nous pouvons imaginer qu'il est
  possible de construire, sur base de ces deux instructions, un
  programme qui calcule la (partie entière de la) moyenne de deux
  nombres, en utilisant d'abord \inasm{add}\ pour faire la somme de
  ces deux nombres, puis \inasm{idiv}\ pour diviser le résultat par
  $2$. Naturellement, la somme doit être effectuée avant la division:
  l'ordre (la séquence) des instructions est donc important. Ce
  faisant, nous avons esquissé l'écriture d'un programme, qui
  calculerait une moyenne en utilisant \inasm{add}\ et \inasm{idiv}.
\item Le \emph{programme de l'ordinateur peut être
    modifié}. L'ordinateur n'a pas été conçu dans le but d'exécuter un
  et un seul programme, mais son utilisateur a la faculté de modifier
  le programme, soit en acquérant des programmes écrits par des tiers
  (comme des applications achetées et téléchargées sur Internet, par
  exemple), soit en écrivant lui-même de nouveaux programmes.
\item Enfin, \emph{l'information} que l'ordinateur traite à l'aide des
  programmes qu'il exécute est \emph{représentée de manière
    numérique}, et est traitée comme telle. Cela signifie que toute
  information, quelque soit sa nature (nombres, texte, images, sons,
  vidéos,\ldots) doit être exprimée sous forme de nombres pour pouvoir
  être traitée par l'ordinateur. En l'occurrence, tous les ordinateurs
  modernes représentent l'information sous forme de nombres
  \emph{binaires}, c'est-à-dire formés de séquences de $0$ et de $1$
  uniquement, comme nous le verrons en détails dans le
  Chapitre~\ref{cha:info}. Cela signifie également que les
  \emph{traitements} exécutés (et exprimés à l'aide des instructions
  connues de l'ordinateur) réalisent eux aussi des opérations sur des
  nombres (ce sont des \emph{opérations logiques et arithmétiques}).
\end{enumerate}

Ces éléments constituent les caractéristiques généralement retenues
pour définir un \emph{ordinateur}:
\begin{inparaenum}[(1)]
\item la possibilité d'exécuter un programme arbitraire, qui peut être
  modifié, et est donc stocké dans la mémoire de l'ordinateur au même
  titre que les données qui sont traitées\footnote{Il doit également
    être possible d'écrire des programmes complexes, comprenant des
    boucles et des sauts. La notion précise est la notion de machine
    Turing-complète, telle que définie par le mathématicien anglais
    Alan \textsc{Turing} \cite{Turing36}. Nous ne développerons pas
    cette notion en détail ici, le lecteur intéressé peut consulter
    une introduction à la calculabilité comme l'excellent ouvrage de
    Pierre \textsc{Wolper} \cite{wolper06}.}; et
\item la représentation et le
  traitement numérique de l'information (y compris du programme à
  exécuter).
\end{inparaenum}

\section{Un premier modèle: l'architecture de \textsc{von Neumann}}
Maintenant que nous savons \emph{ce que fait} un ordinateur, nous
devons répondre à la question du \emph{comment ?} Dans une première
approche, nous allons partir d'un exemple de programme, emprunté au
cours de Programmation (INFO-F-101). Il s'agit d'un exemple écrit en
Python, et qui calcule les éventuelles racines réelles d'une équation
du second degré à une inconnue. Il est donné à la
\figurename~\ref{fig:ex-prog}.

\begin{figure}
  \centering
  \lstinputlisting[language=python]{chapters/01-exemple.py}
   \caption{Un exemple de programme écrit en langage Python.\label{fig:ex-prog}}
\end{figure}


Cet exemple est fort utile car il illustre bien les différentes
ressources dont l'ordinateur a besoin pour exécuter un programme. En
voici la liste:
\begin{itemize}
\item L'ordinateur doit disposer d'un moyen d'\emph{obtenir des données
    en entrée} (c'est-à-dire les données à traiter). Dans notre
  exempele, le mot-clé \lstinline{input} interroge l'utilisateur qui
  entre les données \textit{via} le clavier. Ces données sont stockées
  dans des variables \lstinline{a}, \lstinline{b} et~\lstinline{c}.
\item L'ordinateur doit être capable de stocker des données de manière
  temporaire, c'est-à-dire durant la durée d'exécution d'un
  programme. Ces données temporaires correspondent en général aux
  données d'entrée, ou aux résultats de calculs intermédiaires. Dans
  notre exemple, les variables \lstinline{a}, \lstinline{b},
  \lstinline{c} et \lstinline{delta} représentent ces stockages
  temporaires. Notez bien que \lstinline{delta} n'est pas une donnée
  d'entrée du programme, mais bien une valeur intermédiaire du calcul.
\item L'ordinateur doit pouvoir communiquer ses résultats au monde
  extérieur. Dans notre exemple, le mot-clé \lstinline{print} permet
  d'afficher des messages ainsi que certaines variables.
\item L'ordinateur doit avoir la capacité d'effectuer certaines
  opérations de base, sans que l'utilisateur n'ait besoin d'écrire un
  programme pour expliciter à l'ordinateur le sens de ces opérations
  de base. C'est le cas, par exemple des opérations arithmétiques:
  elles sont considérées comme <<~connues~>> et peuvent être utilisées
  dans un programme sans qu'il soit nécessaire d'expliciter le calcule
  du \lstinline{+}, du \lstinline{-}, \textit{etc}. Dans notre
  exemple, ces opérations interviennent dans le calcul du~$\Delta$.

  L'ensemble des opérations de base connues de l'ordinateur, et qu'on
  peut utiliser pour écrire un programme, composent ce qu'on appelle
  un \emph{langage}. Le langage principal d'un ordinateur est ce qu'on
  appelle le \emph{langage machine}. C'est ce langage qui opère
  directement sur la représentation binaire des informations. Notre
  exemple, lui, est écrit dans une langage différent: le langage
  Python. Dans ce langage (et donc dans notre exemple), on ne voit pas
  la représentation binaire des informations apparaître
  explicitement\footnote{Par exemple, la valeur $2$ utilisé dans la
    dernière ligne du programme n'est pas exprimée en binaire, sans
    quoi on aurait écrit $10$ (voir Chapitre~\ref{cha:info})}. Au
  contraire, Python permet d'exprimer les opérations sur des données
  plus \emph{abstraites}, et donc plus facilement compréhensible par
  un être humain, comme le texte qui est affiché pour présenter le
  résultat. Le langage Python offre donc une facilité accrue par
  rapport au langage machine, mais afin qu'il puisse être exécuté par
  l'ordinateur, il devra impérativement être traduit en langage
  machine.

\item Bien que cela apparaisse de manière moins évidente sur notre
  exemple, l'ordinateur doit également être capable de \emph{stocker
    le programme à exécuter} (puisque celui-ci peut être modifié) pour
  pouvoir s'y référer, et ainsi avoir la capacité d'accéder à la bonne
  instruction à exécuter. Pour ce faire, il faut aussi disposer d'un
  moyen retenant l'avancement de l'exécution dans le programme, et de
  modifier cet état d'avancement. Cette modification peut consister
  soit à passer à l'instruction suivante dans le programme, soit à
  désigner une instruction arbitraire comme étant celle à exécuter en
  prochain lieu, éventuellement selon certaines conditions (c'est ce
  que fait l'instruction \lstinline{if} dans notre exemple).
\end{itemize}


\begin{wrapfigure}{r}{0.3\textwidth}
  \centering
  \includegraphics[width=.22\textwidth]{images/Chap1/VonNeuman.jpg}
  \caption*{John \textsc{von Neumann}.}
  % http://upload.wikimedia.org/wikipedia/commons/5/5e/JohnvonNeumann-LosAlamos.gif
  \label{fig:vonneumann}
\end{wrapfigure}
Sur base de ces besoins, on peut proposer un premier modèle
\emph{abstrait} de ce qui est nécessaire pour concevoir une telle
machine. Notre modèle est présenté à la
\figurename~\ref{fig:vnarch}. Il s'agit d'une conception de
l'ordinateur qui a été proposée par John \textsc{von
  Neumann}\footnote{Mathématicien hongrois, né à Budapest le 28
  décembre 1903, mort à Washington, le 8 février 1957.} en 1945
\cite{vn45}, et sur lequel tous les ordinateurs modernes se basent.
Il est bien entendu que ce modèle est une simplification (sans doute
un peu grossière) de la réalité, mais elle est suffisante, en première
approximation, pour expliquer les principes généraux de fonctionnement
de l'ordinateur. Nous raffinerons cette figure dans la suite. Notons
que ce modèle est souvent appelé \emph{architecture de von Neumann} ou
\emph{architecture à programme enregistré} (\textit{stored program
  architecture} en anglais). On utilise ici le terme architecture, car
le modèle explique quels sont les différents composants de
l'ordinateur et comment ils sont utilisés, interconnectés, pour bâtir
la machine qu'est l'ordinateur.

En analysant cette figure, nous pouvons constater:
\begin{itemize}
\item que l'\textsc{\textbf{Ordinateur}} (cadre gras) communique avec
  le monde extérieure à travers des dispositifs appelés
  \emph{périphériques} d'entrée/sortie (par exemple: le clavier, la
  souris, l'écran, \textit{etc});
\item que l'ordinateur est composé essentiellement de deux composants:
  le processeur (ou en anglais, \textit{Central Processing Unit}, CPU)
  et la mémoire. Ce sont les deux cadres aux bords arrondis sur la
  figure; et enfin
\item que le processeur est lui-même composé d'une unité de contrôle,
  d'une unité arithmético-logique (en anglais \textit{Arithmetic and
    Logic Unit}, ALU) et de registres.
\end{itemize}

Détaillons maintenant les rôles de ces différents composants, pour
l'exécution d'un programme.  Tout d'abord, le programme et ses données
sont stockées dans la \emph{mémoire}, qui, comme son nom l'indique,
est un composant servant à mémoriser (stocker) l'information. Comme
cette mémoire est modifiable, on peut également modifier le programme
ou l'appliquer à d'autres données; ce qui est essentiel dans notre
définition d'ordinateur (cela explique le nom d'\textit{architecture à
  programme enregistré}, car le programme est enregistré dans la
mémoire).

Ensuite, le \emph{processeur} a pour tâche d'exécuter le programme,
instruction par instruction. Pour ce faire, il interroge la mémoire
qui lui transmet la prochaine instruction à exécuter; puis, il analyse et
exécute celle-ci, et recommence \textit{ad infinitum}. Le processeur a
donc un comportement \emph{cyclique} que l'on représente par la
\emph{boucle d'interprétation du processeur} parfois appelée également
\textit{fetch--decode--execute}, du nom des trois étapes principales
(en anglais):
  \begin{enumerate}
  \item \textit{fetch}: aller chercher la prochaine instruction à
    exécuter, en mémoire. Cela signifie, comme nous l'avons déjà
    dit, que le processeur doit avoir un moyen, d'identifier cette
    instruction parmi d'autres en mémoire. C'est un des rôles (mais
    pas le seul) des registres.
  \item \textit{decode}: il s'agit ici d'analyser la représentation
    binaire de l'instruction (en \emph{langage machine}) pour en
    déduire l'opération à effectuer.
  \item \textit{execute}: exécuter l'instruction à proprement
    parler. Les éléments du CPU en charge de l'exécution d'une
    instruction sont les registres et l'ALU. D'une part, les registres
    sont des zones de mémoire de très petite capacité mais d'accès
    très rapide, dans lesquels les données nécessaires à l'exécution
    de l'instruction, ainsi que son résultat, seront stockés. D'autre
    part, l'ALU est un circuit électronique du processeur qui peut
    appliquer une opération arithmétique ou logique à deux données
    provenant des registres. Par exemple, si l'on souhaite faire la
    somme de deux nombres, il faudra placer les deux valeurs dans des
    registres, transmettre ces valeurs à l'ALU qui effectuera la
    somme, puis placer le résultat dans un registre. Nous décrirons ce
    procédé plus en détail à la Section~\ref{sec:le-processeur}, et
    surtout au Chapitre~\ref{cha:micro-arch}.

    L'effet d'une instruction peut également être d'écrire ou de lire
    une valeur en mémoire (depuis ou vers les registres), ou encore de
    communiquer avec les périphériques.
  \end{enumerate}
  La coordination de ces trois étapes est assurée par l'unité de
  contrôle du CPU. Ainsi, l'unité de contrôle doit s'assurer que ce
  sont les bons registres qui transmettent leur données à l'ALU, que
  l'ALU applique la bonne opération aux données (il faut choisir
  l'opération à effectuer parmi une palette d'opérations disponibles),
  que le résultat calculé par l'ALU est placé dans le bon registre de
  sortie, \emph{etc}. 

  Dans le Chapitre~\ref{cha:orga}, nous expliquerons plus en détail le
  fonctionnement de cette boucle. Dans le Chapitre~\ref{cha:interr},
  nous raffinerons cette boucle pour introduire un mécanisme
  d'\emph{interruption}, qui est essentiel au bon fonctionnement des
  ordinateurs modernes.


\begin{figure}
  \centering
  \begin{tikzpicture}%[scale=.9]
    \draw[very thick] (-.5, 1.5) rectangle (7.5, -8) ;
    \node at (3.5, 1) {\textbf{\textsc{Ordinateur}}} ;

    \draw[rounded corners=15pt] (0,0) rectangle (7,-5.5) ;
    \node at (3.5, -.5) {Processeur (CPU)} ;
       
    \draw (.5, -1) rectangle (6.5, -2) ;
    \draw (.5, -2.5) rectangle (6.5, -3.5) ;
    \draw (.5, -4) rectangle (6.5, -5) ;
    
    \node at (3.5, -1.5) { Unité de contrôle } ;
    \node at (3.5, -3) { Unité arithmético-logique (ALU) } ;
    \node at (3.5, -4.5) { Registres } ;
    
    \draw[rounded corners=15pt] (0, -6.5) rectangle (7, -7.5) ;
    \node at (3.5, -7) {Mémoire} ;

    \draw[-latex] (3, -6.5) -- (3, -5.5) ;
    \draw[-latex] (4, -5.5) -- (4, -6.5) ;

    \draw (9,-2.5) rectangle (12,-4.5) ;
    \node at (10.5, -3) {Entrées} ;
    \node at (10.5, -4) {Sorties} ;
    \node at (10.5, -3.5) {/} ;
     
    \draw[-latex] (9, -3) -- (7.5, -3) ;
    \draw[-latex] (7.5, -4) -- (9, -4) ;

    
  \end{tikzpicture}
  \caption{L'architecture \textsc{von Neumann}, un premier modèle
    d'ordinateur.}
  \label{fig:vnarch}
\end{figure}

\exemple{ Dans ces notes de cours, nous utiliserons souvent la famille
  de processeurs x86 de la firme Intel comme exemples pour illustrer
  les notions relatives aux processeurs. On retrouve aujourd'hui ces
  processeurs dans l'immense majorité des ordinateurs personnels.

  En particulier, nous considérerons le processeur i486. Il s'agit
  d'un processeur qui a été commercialisé de 1989 à 2007, et il a des
  performances relativement modestes si on le compare aux processeurs
  récents. Néanmoins, il possède la plupart des caractéristiques des
  processeurs modernes, tout en restant relativement simple. Il
  constitue donc un excellent exemple pédagogique.\tbc  }

\exemplesuite{
  L'architecture de l'i486 est montrée à la
  \figurename~\ref{fig:i486-arch}. Comme on peut le voir, elle est
  bien plus complexe que notre simple modèle de la
  \figurename~\ref{fig:vnarch} ! Mais nous pouvons déjà reconnaître
  certains éléments:
  \begin{itemize}
  \item En jaune, sur la gauche de la figure, nous retrouvons l'ALU
    (avec le \textit{shifter} qui peut également être vu comme un
    circuit réalisant des opérations arithmétiques), et les registres
    servant à contenir des données (l'ensemble de ces registres est
    appelé \textit{register file}). L'i486 possède au total 32
    registres qui ont des fonctions différentes. En particulier, les 4
    registres appelés \inasm{eax}, \inasm{ebx}, \inasm{ecx} et
    \inasm{edx} servent à stocker les données sur lesquelles opèrent
    les instructions. Ce sont des registres de 32 bits. L'i486 possède
    également 8 registres spécialisés de 80 bits, qui permettent
    d'effectuer des opérations arithmétiques sur des nombres décimaux
    (dont nous parlerons dans la Section~\ref{sec:repr-ieee754}). Ces
    opérations un peu spéciales ne sont pas réalisées par l'ALU, mais
    par le FPU (pour \textit{floating point unit}), en orange, sur la
    gauche de la figure.
  \item Les flèches en noires à droite de l'image représentent les
    communications du processeur avec le monde extérieur, c'est-à-dire
    avec la mémoire (deux flèches du haut) et les périphériques.
  \item On reconnait également un bloc chargé du décodage des
    instructions (dans le base de la figure) et un bloc chargé de la
    lecture en mémoire (\textit{fetch}), appelé ici
    \textit{prefetcher}.
  \end{itemize}

}

\begin{sidewaysfigure}
  \centering
  \includegraphics[width=.8\textwidth]{images/Chap1/80486DX2_arch.pdf}
  \caption[L'architecture de l'i486]{L'architecture de l'i486 (sous sa variante 80486DX2), telle
    que publiée par Intel \cite{i486}.}
  \source{ Appaloosa
    (\url{https://commons.wikimedia.org/wiki/File:80486DX2\_arch.svg}),
    <<~80486DX2 arch~>>,
    \url{https://creativecommons.org/licenses/by-sa/3.0/legalcode} }
  \label{fig:i486-arch}
\end{sidewaysfigure}


\section{Une vision différente: structure en niveaux et traductions}\label{sec:une-visi-diff}
Maintenant que nous avons identifié les différents composants d'un
ordinateur et leurs fonctions, nous pouvons approfondir la question du
\emph{comment ?} posée au début de la section précédente. Nous
consacrerons évidemment tout le cours à expliquer \emph{comment} les
différents composants que nous avons identifiés remplissent leur
fonction, mais nous pouvons déjà donner une vue globale qui
correspondra au plan de l'exposé.

\paragraph{Différents niveaux} Partons du CPU. Celui-ci est composé de
circuits électroniques que nous appellerons \emph{circuits logiques},
et que nous étudierons dans quelques leçons. C'est à l'aide de ces
circuits logiques que le CPU doit donc interpréter le langage
machine. Comme nous le verrons plus tard, l'interprétation de chaque
instruction en langage machine n'est pas immédiate: le langage machine
est d'abord traduit dans une langage plus simple encore, le
\emph{microlangage}, lequel est enfin exécuté à l'aide des circuits
logiques. Tout comme un programme (en langage machine) se compose
d'une série d'instructions (appelées \emph{instructions machine}),
chaque instruction machine est traduite\footnote{\`A l'aide d'une
  boucle similaire à la boucle \textit{fetch--decode--execute}.} en
une série de micro-instructions (les instructions du
micro-langage). Nous avons donc identifié les trois \og couches\fg{}
les plus basses de l'ordinateur, à savoir (et en partant de la couche
la plus basse):
\begin{enumerate}
\setcounter{enumi}{-1}
\item les circuits logiques (qui constituent le véritable matériel
  constitutif de l'ordinateur);
\item le micro-langage; et
\item le langage machine, qui est le langage du CPU.
\end{enumerate}
Durant ce cours, nous nous concentrerons essentiellement sur ces trois
couches.

Mais tout qui a un jour utilisé un ordinateur sait pertinemment bien
que celui-ci peut être utilisé sans interagir directement avec le
processeur, à l'aide du langage machine. L'exemple de la
\figurename~\ref{fig:ex-prog} est écrit, comme nous l'avons déjà dit,
en Python, un langage plus abstrait (on dit aussi: \og de plus haut
niveau\fg{}) que le langage machine, ce qui est une facilité
non-négligeable. En effet, un même programme en langage Python peut
être exécuté par des ordinateurs différents qui ont potentiellement
des caractéristiques (nombre de registres, langage machine)
différentes. Cela n'a pas d'importance pour le programmeur qui a écrit
son programme en Python, car ce langage ne demande pas et ne permet
pas d'interagir directement avec les composants matériels de base
(ALU, registres) de la machine. Au contraire, si on souhaite exécuter
un programme en Python sur un ordinateur donné, il faut disposer d'un
\emph{interpréteur}, qui est lui-même un programme qui \emph{traduit}
le programme en langage Python vers une séquence d'instruction machine
propre à l'ordinateur sur lequel le programme Python doit être
exécuté.


Par ailleurs, tout ordinateur personnel, tout \textit{smartphone} est
aujourd'hui livré avec un programme de base, permettant d'interagir
facilement avec lui, et appelé \og système d'exploitation\fg{}, ou
\textit{operating system}, ou encore OS (par exemple, Windows, Linux,
MacOS, Android, iOS, \textit{etc}). Tous ces programmes sont
\textit{in fine} exécutés par le processeur et doivent donc être
\emph{traduits} en langage machine. Au final, nous avons 3 couches
supplémentaires qui s'empilent au-dessus du langage machine:
\begin{enumerate}
\setcounter{enumi}{2}
\item le système d'exploitation, dont nous parlerons brièvement à
  la fin du cours, mais qui fera surtout l'objet d'un cours de
  deuxième année;
\item l'assembleur, qui est un langage intermédiaire entre le
  langage machine et les programmes écrits dans un langage de haut
  niveau. L'assembleur sera étudié dans le cours de langages de
  programmation, au second quadrimestre;
\item les langages de haut niveau, qui sont ceux à l'aide desquels
  on écrit les applications que l'on utilise quotidiennement
  (traitement de texte, navigateur web, \textit{etc}). Le cours de
  programmation vous enseigne un de ces langages (python) et le cours
  de langages de programmation (second quadrimestre) vous en
  enseignera d'autres.
\end{enumerate}
\bigskip

De bout en bout, l'exécution d'un programme de haut niveau peut donc
se résumer comme suit: le programme de haut niveau (niveau 5) est
traduit en assembleur (niveau 4). L'assembleur est traduit en un
langage qui est essentiellement du langage machine augmenté d'appels
aux services prodigués par le système d'exploitation (niveau 3). Ces
\og appels systèmes\fg{} sont eux-mêmes traduits en langage machine,
et on obtient alors, au niveau 2, un programme entièrement en langage
machine. L'exécution de ce programme machine par le CPU consiste en
une traduction vers le micro-langage interne au CPU (niveau 1), dont
le résultat est finalement exécuté à l'aide de circuits logiques
(niveau 0).

\paragraph{Interprétation et Compilation} On voit donc qu'on a affaire
à une chaîne de traductions successives. Celles-ci peuvent être
effectuées de deux façon différentes:

\begin{itemize}
\item soit un programme d'un certain niveau est intégralement traduit
  en un programme du niveau inférieur, avant le début de
  l'exécution. On parle alors de \emph{compilation}. Le résultat de la
  traduction peut être stocké et réutilisé, mais ne sera plus modifié
  durant l'exécution. La compilation est réalisée à l'aide d'un
  programme appelé \emph{compilateur}.

  C'est le cas des applications que nous
  téléchargeons sur Internet (que ce soit pour un ordinateur de bureau
  ou pour un \textit{smartphone}): elles ont été écrites dans un
  langage de haut niveau par leurs concepteurs, puis \emph{compilées}
  en langage machine. C'est le résultat de cette compilation qui est
  distribué et téléchargé par les utilisateurs finaux;
\item soit un programme d'un certain niveau est traduit vers le niveau
  inférieur, instruction par instruction, au moment de l'exécution.
  On parle alors \emph{d'interprétation}. C'est ce qui se passe, par
  exemple, quand on traduit le langage machine en micro-code, ou quand
  le processeur exécute le langage machine.

  De manière générale, un \emph{interpréteur} est un programme écrit
  dans un langage de niveau $i$ et qui interprète un programme écrit
  dans un langage de niveau $j>i$, instruction par instruction,
  c'est-à-dire en exécutant continuellement une boucle qui consiste à:
  \begin{inparaenum}[(1)]
  \item lire la prochaine instruction à exécuter;
  \item analyser et exécuter cette instruction;
  \item passer à l'instruction suivante.
  \end{inparaenum}
  Le CPU peut donc être vu comme la réalisation d'un interpréteur pour
  le langage machine, réalise à l'aide de micro-code.
\end{itemize}

La hiérarchie de niveaux que nous venons de décrire, ainsi que les
différents types de traductions entre ces niveaux, sont présentés à la
\figurename~\ref{fig:couches}.
 



\paragraph{Matériel et logiciel} Notons finalement que chacun des 6
niveaux peut théoriquement être réalisé soit à l'aide de
\emph{matériel} soit à l'aide de \emph{logiciel}. \'Elucidons ces deux
termes:
\begin{itemize}
\item le \emph{matériel} (\textit{hardware} en anglais) est l'ensemble
  des dispositifs physiques qui constituent l'ordinateur; tandis que
\item un \emph{logiciel} (\textit{software} en anglais) est une
  représentation informatique des informations nécessaires à un
  traitement réalisé par l'ordinateur, à savoir, la séquence
  d'instructions à exécuter, et les données.
\end{itemize}

En pratique, les niveaux 0,1 et 2 sont réalisés à l'aide de matériel,
et les niveaux 3, 4 et 5 à l'aide de logiciel. Le logiciel et le
matériel ont chacun leurs avantages et leurs inconvénients. Le
logiciel est plus flexible, dans le sens où on peut le modifier et le
remplacer, mais plus lent. Le matériel est plus rapide, mais n'est pas
modifiable, en général. Il est par exemple possible de changer
d'interpréteur Python (niveau 5) si jamais une nouvelle version de ce
langage venait à être proposée. Par contre, le processeur, qui exécute
le langage machine, doit être très rapide, et on ne peut pas modifier
la traduction du langage machine en micro-langage (niveau 2 vers
niveau 1).

% \section{Un peu d'histoire: le premier ordinateur}

\begin{center}
  \aldineleft\vspace*{1cm} \decoone\vspace*{1cm} \aldineright
\end{center}

\begin{figure}
  \centering
  \begin{tikzpicture}[node distance=2cm]
    \node[draw, rectangle, align=center] (n0) {Niveau 0:\\ les portes
      logiques} ;
    \node[draw, rectangle, align=center, above of = n0] (n1) {Niveau 1:\\ le
      micro-code} ;
    \node[draw, rectangle, align=center, above of = n1] (n2) {Niveau 2:\\ le langage
      machine, langage du CPU} ;
    \node[draw, rectangle, align=center, above of = n2] (n3) {Niveau 3:\\ le système
      d'exploitation (Linux, Windows, MacOS\ldots) }
    ;
    \node[draw, rectangle, align=center, above of = n3] (n4) {Niveau 4:\\ l'assembleur} ;
    \node[draw, rectangle, align=center, above of = n4] (n5) {Niveau 5:\\ les langages
      de haut niveau (Python, C, C++,\ldots)} ;
    
    \path[-latex]
    (n1) edge node[right] {Interprétation} (n0) 
    (n2) edge node[right] {Interprétation} (n1) 
    (n3) edge node[right] {Compilation } (n2) 
    (n4) edge node[right] {Compilation (assemblage)} (n3) 
    (n5) edge node[right] {Compilation (appels systèmes)} (n4) ;

    \draw[decorate,decoration={brace,amplitude=10pt},xshift=-2cm,yshift=0pt]
    (n0.south west -| n2.north west) -- (n2.north west) node
    [black,midway,xshift=-2cm, align=right] {Matériel\\ (dans le CPU)};
    
    \draw[decorate,decoration={brace,amplitude=10pt},xshift=-2cm,yshift=0pt]
    (n3.south west) -- (n5.north west -| n3.west) node
    [black,midway,xshift=-2cm, align=right] {Logiciel};
  
  \end{tikzpicture}
  \caption{Les 6 couches décrivant le fonctionnement d'un ordinateur.}
  \label{fig:couches}
\end{figure}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% TeX-engine: xetex
%%% End:
