\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {french}{}
\contentsline {paragraph}{\nonumberline Remerciements}{13}{section*.5}% 
\contentsline {part}{\numberline {I}Notions de base}{15}{part.1}% 
\contentsline {chapter}{\numberline {1}Leçon 1 -- Introduction: Qu'est-ce qu'un ordinateur ?}{17}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Définitions et étymologie}{17}{section.1.1}% 
\contentsline {section}{\numberline {1.2}Un premier modèle: l'architecture de \textsc {von Neumann}}{19}{section.1.2}% 
\contentsline {section}{\numberline {1.3}Une vision différente: structure en niveaux et traductions}{24}{section.1.3}% 
\contentsline {paragraph}{\nonumberline Différents niveaux}{24}{section*.11}% 
\contentsline {paragraph}{\nonumberline Interprétation et Compilation}{27}{section*.12}% 
\contentsline {paragraph}{\nonumberline Matériel et logiciel}{27}{section*.13}% 
\contentsline {chapter}{\numberline {2}Leçons 2 à 4 -- Représentation de l'information}{29}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Unités de quantité d'information}{30}{section.2.1}% 
\contentsline {section}{\numberline {2.2}\'Ecriture des nombres dans différentes bases}{31}{section.2.2}% 
\contentsline {paragraph}{\nonumberline Chiffres de poids fort, de poids faible}{33}{section*.15}% 
\contentsline {paragraph}{\nonumberline Ajouts de zéros}{33}{section*.16}% 
\contentsline {paragraph}{\nonumberline Multiplier ou diviser par la base}{33}{section*.17}% 
\contentsline {paragraph}{\nonumberline Combien de nombres représentables sur $n$ chiffres ?}{34}{section*.18}% 
\contentsline {subsection}{\numberline {2.2.1}Changements de base}{34}{subsection.2.2.1}% 
\contentsline {paragraph}{\nonumberline Cas des nombres entiers}{35}{section*.19}% 
\contentsline {paragraph}{\nonumberline Nombres réels}{36}{section*.20}% 
\contentsline {subsection}{\numberline {2.2.2}Opérations en base 2}{38}{subsection.2.2.2}% 
\contentsline {paragraph}{\nonumberline Opérations arithmétiques}{39}{section*.21}% 
\contentsline {paragraph}{\nonumberline Opérations logiques}{39}{section*.22}% 
\contentsline {paragraph}{\nonumberline Masques}{40}{section*.23}% 
\contentsline {paragraph}{\nonumberline Décalages, division et multiplications}{40}{section*.24}% 
\contentsline {subsection}{\numberline {2.2.3}Représentation des entiers non-signés}{41}{subsection.2.2.3}% 
\contentsline {subsection}{\numberline {2.2.4}Représentation des nombres entiers signés}{41}{subsection.2.2.4}% 
\contentsline {paragraph}{\nonumberline Bit de signe}{42}{section*.25}% 
\contentsline {paragraph}{\nonumberline Complément à 1}{42}{section*.26}% 
\contentsline {paragraph}{\nonumberline Complément à 2}{43}{section*.27}% 
\contentsline {paragraph}{\nonumberline Excès à $K$}{44}{section*.28}% 
\contentsline {paragraph}{\nonumberline Comparaison des différentes représentation}{44}{section*.29}% 
\contentsline {subsection}{\numberline {2.2.5}Représentation des nombres <<~réels~>>}{45}{subsection.2.2.5}% 
\contentsline {paragraph}{\nonumberline Une première technique: la virgule fixe}{45}{section*.31}% 
\contentsline {paragraph}{\nonumberline La virgule flottante: IEEE754}{46}{section*.32}% 
\contentsline {section}{\numberline {2.3}Représentation des caractères}{49}{section.2.3}% 
\contentsline {paragraph}{\nonumberline Codes historiques: \'Emile \textsc {Baudot} et les téléscripteurs}{49}{section*.34}% 
\contentsline {paragraph}{\nonumberline Le code ASCII}{50}{section*.36}% 
\contentsline {paragraph}{\nonumberline Unicode}{50}{section*.39}% 
\contentsline {section}{\numberline {2.4}Représentation d'images}{52}{section.2.4}% 
\contentsline {section}{\numberline {2.5}Représentation des instructions}{54}{section.2.5}% 
\contentsline {section}{\numberline {2.6}Détection et correction d'erreurs}{56}{section.2.6}% 
\contentsline {subsection}{\numberline {2.6.1}Bit de parité}{56}{subsection.2.6.1}% 
\contentsline {subsection}{\numberline {2.6.2}Code de \textsc {Hamming}}{57}{subsection.2.6.2}% 
\contentsline {subsection}{\numberline {2.6.3}Applications des codes correcteurs d'erreur}{60}{subsection.2.6.3}% 
\contentsline {section}{\numberline {2.7}Conclusion: sémantique d'une représentation binaire}{61}{section.2.7}% 
\contentsline {chapter}{\numberline {3}Leçon 5 \& 6 -- Organisation de l'ordinateur}{65}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Mémoire primaire}{65}{section.3.1}% 
\contentsline {subsection}{\numberline {3.1.1}Structure et adresses}{66}{subsection.3.1.1}% 
\contentsline {subsection}{\numberline {3.1.2}Mémoire cache}{67}{subsection.3.1.2}% 
\contentsline {subsection}{\numberline {3.1.3}Ordre des octets dans les mots}{68}{subsection.3.1.3}% 
\contentsline {subsection}{\numberline {3.1.4}Réalisation de la mémoire primaire}{70}{subsection.3.1.4}% 
\contentsline {paragraph}{\nonumberline Les tubes de Williams}{70}{section*.46}% 
\contentsline {paragraph}{\nonumberline Les mémoires à lignes à délai}{70}{section*.48}% 
\contentsline {paragraph}{\nonumberline Les mémoires à tores magnétiques}{70}{section*.50}% 
\contentsline {section}{\numberline {3.2}Le processeur}{74}{section.3.2}% 
\contentsline {subsection}{\numberline {3.2.1}Composants du processeur}{74}{subsection.3.2.1}% 
\contentsline {subsection}{\numberline {3.2.2}Le chemin des données -- \textit {datapath}}{76}{subsection.3.2.2}% 
\contentsline {subsection}{\numberline {3.2.3}Exécution des instructions}{77}{subsection.3.2.3}% 
\contentsline {subsection}{\numberline {3.2.4}Machine à pile}{78}{subsection.3.2.4}% 
\contentsline {paragraph}{\nonumberline La pile comme structure de données}{79}{section*.55}% 
\contentsline {paragraph}{\nonumberline Implémentation d'une pile}{80}{section*.57}% 
\contentsline {paragraph}{\nonumberline Utilisation dans le langage machine}{80}{section*.58}% 
\contentsline {subsection}{\numberline {3.2.5}Choix du jeu d'instructions machine}{80}{subsection.3.2.5}% 
\contentsline {subsection}{\numberline {3.2.6}Techniques pour améliorer l'efficacité des processeurs}{82}{subsection.3.2.6}% 
\contentsline {paragraph}{\nonumberline Exécution en parallèle des instructions}{82}{section*.61}% 
\contentsline {paragraph}{\nonumberline Architectures superscalaires}{83}{section*.63}% 
\contentsline {paragraph}{\nonumberline Processeurs vectoriels ou SIMD}{84}{section*.66}% 
\contentsline {paragraph}{\nonumberline Multiprocesseurs}{84}{section*.69}% 
\contentsline {paragraph}{\nonumberline Systèmes multi-c\oe urs}{88}{section*.71}% 
\contentsline {paragraph}{\nonumberline Grappes et fermes de calcul}{88}{section*.72}% 
\contentsline {section}{\numberline {3.3}Les périphériques}{89}{section.3.3}% 
\contentsline {subsection}{\numberline {3.3.1}Mémoire secondaire}{89}{subsection.3.3.1}% 
\contentsline {paragraph}{\nonumberline Les cartes perforées}{90}{section*.73}% 
\contentsline {paragraph}{\nonumberline Les Rubans perforés}{90}{section*.75}% 
\contentsline {paragraph}{\nonumberline Les bandes magnétiques}{91}{section*.77}% 
\contentsline {paragraph}{\nonumberline Les disques magnétiques}{91}{section*.78}% 
\contentsline {paragraph}{\nonumberline Les disques optiques}{92}{section*.80}% 
\contentsline {paragraph}{\nonumberline La mémoire Flash / SSD}{92}{section*.81}% 
\contentsline {subsection}{\numberline {3.3.2}Les périphériques d'entrée/sortie}{92}{subsection.3.3.2}% 
\contentsline {paragraph}{\nonumberline Accès direct à la mémoire}{94}{section*.83}% 
\contentsline {paragraph}{\nonumberline Bus dédiés}{94}{section*.84}% 
\contentsline {part}{\numberline {II}Les portes logiques}{97}{part.2}% 
\contentsline {chapter}{\numberline {4}Leçons 6 à 9 -- Niveau 0: portes logiques}{99}{chapter.4}% 
\contentsline {section}{\numberline {4.1}L'algèbre Booléenne}{99}{section.4.1}% 
\contentsline {paragraph}{\nonumberline Tables de vérité}{100}{section*.86}% 
\contentsline {paragraph}{\nonumberline L'opérateur { \textbf {et}}\xspace }{100}{section*.87}% 
\contentsline {paragraph}{\nonumberline L'opérateur { \textbf {ou}}\xspace }{101}{section*.88}% 
\contentsline {paragraph}{\nonumberline L'opérateur \emph {ou exclusif}}{101}{section*.89}% 
\contentsline {paragraph}{\nonumberline L'opérateur { \textbf {non}}\xspace }{101}{section*.90}% 
\contentsline {paragraph}{\nonumberline Autres opérateurs}{101}{section*.91}% 
\contentsline {paragraph}{\nonumberline Priorité des opérateurs}{102}{section*.92}% 
\contentsline {paragraph}{\nonumberline Identités remarquables}{102}{section*.93}% 
\contentsline {paragraph}{\nonumberline Des expressions Booléennes aux tables de vérité}{103}{section*.94}% 
\contentsline {paragraph}{\nonumberline Des tables de vérité aux expressions Booléennes}{104}{section*.95}% 
\contentsline {section}{\numberline {4.2}Les circuits logiques}{106}{section.4.2}% 
\contentsline {subsection}{\numberline {4.2.1}Les portes logiques en pratique}{106}{subsection.4.2.1}% 
\contentsline {paragraph}{\nonumberline Aspects historiques}{107}{section*.98}% 
\contentsline {paragraph}{\nonumberline Les circuits logiques aujourd'hui, et demain\ldots }{108}{section*.100}% 
\contentsline {subsection}{\numberline {4.2.2}De la formule Booléenne au circuit logique}{109}{subsection.4.2.2}% 
\contentsline {section}{\numberline {4.3}Circuits pour réaliser l'arithmétique binaire}{110}{section.4.3}% 
\contentsline {subsection}{\numberline {4.3.1}Circuit additionneur}{111}{subsection.4.3.1}% 
\contentsline {subsection}{\numberline {4.3.2}Demi-additionneur}{112}{subsection.4.3.2}% 
\contentsline {paragraph}{\nonumberline Additionneur}{112}{section*.102}% 
\contentsline {paragraph}{\nonumberline Additionneur $n$ bits}{114}{section*.104}% 
\contentsline {subsection}{\numberline {4.3.3}Décalage}{115}{subsection.4.3.3}% 
\contentsline {paragraph}{\nonumberline Extensions}{116}{section*.107}% 
\contentsline {subsection}{\numberline {4.3.4}Décodeur}{117}{subsection.4.3.4}% 
\contentsline {subsection}{\numberline {4.3.5}Le <<~sélecteur~>>}{118}{subsection.4.3.5}% 
\contentsline {subsection}{\numberline {4.3.6}ALU simplifiée (1 bit)}{119}{subsection.4.3.6}% 
\contentsline {subsection}{\numberline {4.3.7}ALU $n$ bits}{121}{subsection.4.3.7}% 
\contentsline {paragraph}{\nonumberline Remarque concernant les reports}{124}{section*.115}% 
\contentsline {section}{\numberline {4.4}Circuits pour réaliser des mémoires}{125}{section.4.4}% 
\contentsline {subsection}{\numberline {4.4.1}Mémoire élémentaire}{125}{subsection.4.4.1}% 
\contentsline {subsection}{\numberline {4.4.2}Bascules}{126}{subsection.4.4.2}% 
\contentsline {paragraph}{\nonumberline Bascule simple}{126}{section*.117}% 
\contentsline {paragraph}{\nonumberline Utilité d'une horloge}{128}{section*.119}% 
\contentsline {paragraph}{\nonumberline Bascule avec horloge}{129}{section*.120}% 
\contentsline {paragraph}{\nonumberline Bascule $D$}{129}{section*.122}% 
\contentsline {subsection}{\numberline {4.4.3}Flip-flops}{130}{subsection.4.4.3}% 
\contentsline {paragraph}{\nonumberline Générateur de pulsation}{130}{section*.125}% 
\contentsline {paragraph}{\nonumberline Flip-flop}{131}{section*.126}% 
\contentsline {subsection}{\numberline {4.4.4}Un circuit de mémoire complet}{131}{subsection.4.4.4}% 
\contentsline {part}{\numberline {III}Le micro-langage}{137}{part.3}% 
\contentsline {chapter}{\numberline {5}Leçons 10 à 12 -- La microarchitecture}{139}{chapter.5}% 
\contentsline {paragraph}{\nonumberline Brève perspective historique}{139}{section*.132}% 
\contentsline {paragraph}{\nonumberline Contenu du chapitre}{140}{section*.134}% 
\contentsline {section}{\numberline {5.1}Le \textit {datapath} du MIC-1}{140}{section.5.1}% 
\contentsline {subsection}{\numberline {5.1.1}Composants du Mic-1}{140}{subsection.5.1.1}% 
\contentsline {paragraph}{\nonumberline Registres}{140}{section*.136}% 
\contentsline {paragraph}{\nonumberline ALU et \textit {shifter}}{144}{section*.137}% 
\contentsline {subsection}{\numberline {5.1.2}Cycle d'exécution}{144}{subsection.5.1.2}% 
\contentsline {subsection}{\numberline {5.1.3}Micro-instructions}{146}{subsection.5.1.3}% 
\contentsline {paragraph}{\nonumberline Syntaxe pour le microlangage}{147}{section*.138}% 
\contentsline {section}{\numberline {5.2}Le langage machine \textsc {ijvm}\xspace }{148}{section.5.2}% 
\contentsline {subsection}{\numberline {5.2.1}Instructions du langage}{148}{subsection.5.2.1}% 
\contentsline {subsection}{\numberline {5.2.2}Représentation d'un programme \textsc {ijvm}\xspace en mémoire}{150}{subsection.5.2.2}% 
\contentsline {section}{\numberline {5.3}Implémentation de l'\textsc {ijvm}\xspace sur le MIC1}{152}{section.5.3}% 
\contentsline {subsection}{\numberline {5.3.1}Structure de l'interpréteur}{152}{subsection.5.3.1}% 
\contentsline {subsection}{\numberline {5.3.2}Lecture et décodage}{153}{subsection.5.3.2}% 
\contentsline {subsection}{\numberline {5.3.3}Représentation de la pile en mémoire}{154}{subsection.5.3.3}% 
\contentsline {subsection}{\numberline {5.3.4}Microcode de l'interpréteur}{155}{subsection.5.3.4}% 
\contentsline {paragraph}{\nonumberline Instruction \texttt {NOP}}{155}{section*.145}% 
\contentsline {paragraph}{\nonumberline Instructions \texttt {IADD} et \texttt {ISUB}}{155}{section*.146}% 
\contentsline {paragraph}{\nonumberline Instruction \texttt {POP}}{155}{section*.147}% 
\contentsline {paragraph}{\nonumberline Instruction \texttt {BIPUSH}}{157}{section*.148}% 
\contentsline {paragraph}{\nonumberline Instruction \texttt {GOTO}}{157}{section*.149}% 
\contentsline {paragraph}{\nonumberline L'instruction \texttt {IFEQ}}{158}{section*.150}% 
\contentsline {subsection}{\numberline {5.3.5}L'unité de contrôle du Mic-1}{158}{subsection.5.3.5}% 
\contentsline {subsection}{\numberline {5.3.6}Le \textit {Control Store}}{160}{subsection.5.3.6}% 
\contentsline {subsection}{\numberline {5.3.7}Le séquenceur}{161}{subsection.5.3.7}% 
\contentsline {section}{\numberline {5.4}Pour aller plus loin\ldots }{161}{section.5.4}% 
\contentsline {part}{\numberline {IV}Le langage Machine}{163}{part.4}% 
\contentsline {chapter}{\numberline {6}Leçon 13 -- Langage machine}{165}{chapter.6}% 
\contentsline {paragraph}{\nonumberline Remarque importante}{165}{section*.153}% 
\contentsline {section}{\numberline {6.1}Notion d'architecture}{166}{section.6.1}% 
\contentsline {paragraph}{\nonumberline Modèle mémoire}{167}{section*.154}% 
\contentsline {paragraph}{\nonumberline Registres}{169}{section*.155}% 
\contentsline {paragraph}{\nonumberline Types de données}{169}{section*.156}% 
\contentsline {section}{\numberline {6.2}Instructions machine typiques}{171}{section.6.2}% 
\contentsline {paragraph}{\nonumberline Mouvements de données}{171}{section*.158}% 
\contentsline {paragraph}{\nonumberline Instructions arithmétiques, logiques, etc}{171}{section*.159}% 
\contentsline {paragraph}{\nonumberline Instructions de saut, comparaison et branchement}{173}{section*.161}% 
\contentsline {paragraph}{\nonumberline Appels de procédures}{173}{section*.163}% 
\contentsline {paragraph}{\nonumberline Boucles}{176}{section*.165}% 
\contentsline {paragraph}{\nonumberline Entrées/sorties}{177}{section*.167}% 
\contentsline {section}{\numberline {6.3}Mécanismes de protection}{177}{section.6.3}% 
\contentsline {section}{\numberline {6.4}Représentation des instructions et des opérandes}{179}{section.6.4}% 
\contentsline {subsection}{\numberline {6.4.1}Opcodes à taille variable}{179}{subsection.6.4.1}% 
\contentsline {subsection}{\numberline {6.4.2}Adressage}{181}{subsection.6.4.2}% 
\contentsline {paragraph}{\nonumberline Adressage immédiat}{181}{section*.168}% 
\contentsline {paragraph}{\nonumberline Adressage direct}{181}{section*.169}% 
\contentsline {paragraph}{\nonumberline Adressage par registre}{181}{section*.170}% 
\contentsline {paragraph}{\nonumberline Adressage par registre avec indirection}{181}{section*.171}% 
\contentsline {paragraph}{\nonumberline Adressage indexé}{182}{section*.173}% 
\contentsline {paragraph}{\nonumberline Adressage indexé avec base}{182}{section*.174}% 
\contentsline {chapter}{\numberline {7}Leçon 14 -- Le mécanisme d'interruption}{185}{chapter.7}% 
\contentsline {section}{\numberline {7.1}Exemple introductif}{185}{section.7.1}% 
\contentsline {section}{\numberline {7.2}Déroulement d'une interruption}{187}{section.7.2}% 
\contentsline {subsection}{\numberline {7.2.1}Interruption interruptibles ?}{188}{subsection.7.2.1}% 
\contentsline {subsection}{\numberline {7.2.2}Boucle d'interprétation modifiée}{188}{subsection.7.2.2}% 
\contentsline {subsection}{\numberline {7.2.3}Retour d'une interruption}{189}{subsection.7.2.3}% 
\contentsline {subsection}{\numberline {7.2.4}Exemple}{191}{subsection.7.2.4}% 
\contentsline {section}{\numberline {7.3}Applications du mécanisme d'interruption}{199}{section.7.3}% 
\contentsline {section}{\numberline {7.4}Extensions possibles}{199}{section.7.4}% 
\contentsline {paragraph}{\nonumberline Interruptions interruptibles}{199}{section*.184}% 
\contentsline {paragraph}{\nonumberline Restauration des registres}{200}{section*.185}% 
\contentsline {section}{\numberline {7.5}Cas de l'Intel 486DX}{200}{section.7.5}% 
\contentsline {paragraph}{\nonumberline Interruptions}{200}{section*.186}% 
\contentsline {paragraph}{\nonumberline Exceptions}{201}{section*.187}% 
\contentsline {paragraph}{\nonumberline Table des vecteurs d'interruption}{201}{section*.188}% 
\contentsline {paragraph}{\nonumberline Passage en mode maître}{201}{section*.189}% 
\contentsline {paragraph}{\nonumberline Retour d'interruption}{202}{section*.190}% 
\contentsline {part}{\numberline {V}Le système d'exploitation}{203}{part.5}% 
\contentsline {chapter}{\numberline {8}Leçon 15 -- Le système d'exploitation}{205}{chapter.8}% 
\contentsline {section}{\numberline {8.1}Nécessité d'un système d'exploitation}{205}{section.8.1}% 
\contentsline {subsection}{\numberline {8.1.1}Quelques repères historiques}{206}{subsection.8.1.1}% 
\contentsline {section}{\numberline {8.2}Principe général de fonctionnement}{206}{section.8.2}% 
\contentsline {subsection}{\numberline {8.2.1}Tâches dévolues au système d'exploitation}{206}{subsection.8.2.1}% 
\contentsline {subsection}{\numberline {8.2.2}Mécanismes de protection et de l'appel système}{207}{subsection.8.2.2}% 
\contentsline {subsection}{\numberline {8.2.3}Systèmes d'exploitation orientés utilisateur}{207}{subsection.8.2.3}% 
\contentsline {subsection}{\numberline {8.2.4}Interpréteur de commande}{207}{subsection.8.2.4}% 
\contentsline {subsection}{\numberline {8.2.5}L'odonnanceur}{207}{subsection.8.2.5}% 
\contentsline {subsection}{\numberline {8.2.6}Cas des systèmes d'exploitation temps-réel}{207}{subsection.8.2.6}% 
\contentsline {paragraph}{\nonumberline Pour résumer}{208}{section*.192}% 
\contentsline {section}{\numberline {8.3}Types d'OS}{208}{section.8.3}% 
\contentsline {section}{\numberline {8.4}OS et interruptions}{209}{section.8.4}% 
\contentsline {section}{\numberline {8.5}Appels système}{209}{section.8.5}% 
\contentsline {chapter}{\numberline {9}Leçon 16 et 17 -- Gestion de la mémoire primaire}{211}{chapter.9}% 
\contentsline {section}{\numberline {9.1}Pagination}{212}{section.9.1}% 
\contentsline {subsection}{\numberline {9.1.1}Travail du MMU}{213}{subsection.9.1.1}% 
\contentsline {section}{\numberline {9.2}Pagination à la demande}{214}{section.9.2}% 
\contentsline {paragraph}{\nonumberline Remarques}{215}{section*.193}% 
\contentsline {section}{\numberline {9.3}Exemple: mémoire virtuelle l'Ultra Sparc III}{216}{section.9.3}% 
\contentsline {chapter}{\numberline {10}Leçon 18 -- Gestion des processus et de la mémoire secondaire}{219}{chapter.10}% 
\contentsline {section}{\numberline {10.1}Gestion des processus}{219}{section.10.1}% 
\contentsline {subsection}{\numberline {10.1.1}Cycle de vie d'un processus}{219}{subsection.10.1.1}% 
\contentsline {subsection}{\numberline {10.1.2}Systèmes en \textit {Time sharing}}{221}{subsection.10.1.2}% 
\contentsline {section}{\numberline {10.2}Entrées/sorties virtuelles et systèmes de fichiers}{222}{section.10.2}% 
\contentsline {subsection}{\numberline {10.2.1}Fichiers}{222}{subsection.10.2.1}% 
\contentsline {paragraph}{\nonumberline Problèmes liés à la politique par blocs: fragmentation}{223}{section*.194}% 
\contentsline {subsection}{\numberline {10.2.2}Répertoires}{223}{subsection.10.2.2}% 
\contentsline {subsection}{\numberline {10.2.3}Quelques remarques}{224}{subsection.10.2.3}% 
