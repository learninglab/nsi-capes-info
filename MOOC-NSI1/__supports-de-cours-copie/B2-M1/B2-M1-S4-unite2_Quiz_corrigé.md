# Quiz à propos de l'exercice de _Playlistes_

1. Que renvoie l'interprète Python lorqu'on crée cette durée : `Duree(12, 754, 8766577)`
    
    bonne réponse : `Duree(2459, 43, 37)`

2. Vrai ou Faux ? `Duree(12, 666, 3256)` est plus long que 24h ?

    bonne réponse : Vrai (il s'agit `Duree(24, 0, 16)` donc plus long de 16 secondes)

3. Que donne `Duree(0, 23, 56) + Duree(1, 36, 32)` (répondre quelque chose comme `Duree(...)`) ?

    bonne réponse : `Duree(2, 0, 28)`

4. Que vaut `PINK_FLOYD[56].titre` (on ne mettra pas les délimiteurs de chaîne de caractères dans la réponse) ?

    bonne réponse (on pourra ne pas tenir compte de la casse) : `Another Brick in the Wall, Part III`

5. Quelle est la durée total des titres de cette base (donner la réponse sous la forme d'un objet `Duree`) ? 

    bonne réponse : `Duree(12, 55, 23)`

6. Combien de titres les trois ami-es ont en commun ?

    bonne réponse : 44

7. De quel album est tiré le titre commun le plus long en durée ?

    bonne réponse : `Atom Heart Mother`

8. Bob possède 10 titres dans sa _playlist_ que ni Xuan, ni Inaya n'ont. Quelle année est la plus représentée dans dans cette petite sélection ?

    bonne réponse : 1969