Convertir les nombres suivants en hexadécimal. Pour indiquer que le nombre est en hexadécimal, on écrit 0x devant. Par exemple 42 = 0x2A. C'est la notation retenue, avec des lettres majuscules, pour cet exercice.

D'autres notations existent : 16#2A en automatisme ou (2A)<sub>16</sub> ailleurs.


100

R. 0x64

18

R. 0x12

192

R. 0xC0

255

R. 0xFF

4100

R. 0x1004

Il est important de bien maîtriser la notation hexadécimale des nombres, elle est très utilisée en réseau.

--------------------------------

La conversion analogique numérique permet de transformer un signal analogique (tout signal physique est analogique) en signal numérique composé de 1 et de 0.

Voici un exemple de signal analogique.
![courbe analogique](courbe_analogique.png)

Le convertisseur analogique numérique a une fréquence de 50 kHz et une résolution de 3 bits. Sa technologie par approximation successive fait que, pour chaque échantillon, c'est la valeur numérique immédiatement inférieure qui est choisie et non la plus proche.

Quelle est la période d'échantillonnage ?

R : 20 µs
La période est l'inverse de la fréquence

Quelle est la valeur numérique du premier échantillon (pour t=0) ?

R : 1
La valeur numérique immédiatement inférieure est 1 = 0b001.

Quelle est la valeur numérique du dernier échantillon (pour t=160 µs) ?

R : 7
La valeur numérique immédiatement inférieure est 7 = 0b111

Donner la suite de bits représentant le signal dans l'ordinateur, après la  conversion. Ne pas mettre d'espace entre les échantillons.

R : 001011100101100011100101111
On prend la valeur en binaire de chaque échantillon, c'est comme cela que le signal est présent dans l'ordinateur.

Donner le signal en une suite de nombre hexadécimaux. Si la chaine n'est pas un mutiple de 8, on ajoute le nombre de zéros nécessaire en fin de chaine.
Les nombres sont notés avec 0x devant et séparés par des espaces.

R : 0x2E 0x58 0xE5 0xE0

C'est de cette manière qu'apparaîtrait le signal sur une carte mémoire ou sur un lecteur


--------------------------------
Le signal électrique circule à peu près à 2.10<sup>8</sup> m/s sur le cuivre.
Quel est le temps minimal pour qu'un signal parcourt la distance Paris-Tokyo (10 000 km) ?

R : 50 ms

temps = distance / célérité = 10.10<sup>6</sup> / 2.10<sup>8</sup> = 5.10<sup>-2</sup> s
Attention aux unités

Le temps d'aller retour de 100 ms au mieux ne permet pas d'envisager une communication téléphonique de très grande qualité.

-----------------------------------
Le signal électrique circule à peu près à 2.10<sup>8</sup> m/s sur le cuivre.
Quel est le temps minimal pour qu'un signal aille d'un bout à l'autre de la voiture (5 m) ?

R : 25 ns

temps = distance / célérité = 5 / 2.10<sup>8</sup> = 25.10<sup>-9</sup> s

Attention aux unités
25 ns n'est pas négligeable pour les microcontrôleurs embarqués dans les voitures.

---------------------------------------

Combien de temps faut-il, en secondes, pour télécharger un fichier de 10 Go avec un débit de 100 Mbit/s ?

R : 800 s
durée = Nb_de_bits / débit
durée = 8 * 10.10<sup>9</sup> s