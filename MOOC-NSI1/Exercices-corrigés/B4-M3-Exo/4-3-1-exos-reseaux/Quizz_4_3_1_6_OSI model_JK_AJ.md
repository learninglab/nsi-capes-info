1. Quel est le nom de la couche 2 du modèle OSI
- Liaison de données
- Physique
- Réseau 
- Transport 

R: Liaison de données
La couche 1 est la couche physique, en charge de l'émission des bits, de la synchronisation des horloge.
La couche 2 est en charge de l'émission d'une trame (adresses, gestion des éventuelles priorités, détection d'erreurs)
Les couches 1 et 2 sont traitées habituellement de manière matérielle (hardware).
Les couches 3 et 4, réseau et transport sont généralament traitées de manière logicielle.

2. Quel est le nom de la couche 3 du modèle OSI
- Transport
- Physique
- Liaison de données
- Réseau

R: Réseau 
La couche réseau est en charge de l'acheminement des paquets à travers les différents sous-réseaux. Sur Internet, c'est la couche centrale, d'où le nom de sa couche réseau : IP comme Internet Protocol.

3. Quel est le nom de la couche 4 du modèle OSI
- Liaison de données
- Physique
- Réseau 
- Transport 

R: Transport
La couche Transport a plusieurs missions, la segmentation des messages en paquets acheminables par la couche réseau notamment. Selon le concept des couches du modèle OSI, la couche transport devrait être indépendante de la couche réseau, ce qui n'est pas complètement le cas pour TCP/IP.

4. Quelle est la couche utilisée par un switch pour commuter les trames ?
- Physique, 
- Liaison de données
- Réseau
- Transport

R: Liaison de données
Le swicth utilise les adresses MAC, éléments de la couche liaison de données pour commuter les trames. Le switch ne lit pas le champ de données de la trame Ethernet et n'a donc pas accès aux paquets IP.

5. Quelles sont les couches utilisées par un routeur pour l'acheminement des paquets ?
- Physique, liaison de données
- Physique, liaison de données et réseau
- Liaison de données et réseau
- Réseau et transport

R: Réseau et Transport
Le routeur intervient au niveau de la couche réseau pour acheminer les paquets. Les couches réseau et transport étant très liées sur Internet, c'est par la couche transport que les routeurs gèrent les congestions.
Certains routeurs intègre dans leur boîtier un switch qui lui utilise la couche liaison de données.

6. A quelle couche du modèle OSI se situe la trame ?
- Physique
- Liaison de données
- Réseau
- Transport

R: Liaison de données
La couche physique gère l'émission des bits et la couche liaison de données celle des trames.

7. A quelle couche du modèle OSI se situe le paquet ?
- Physique
- Liaison de données
- Réseau
- Transport

R: Réseau 
Le paquet est l'élément de transport de données qui circule sur Internet, de sous-réseaux en sous-réseaux.

8. A quelle couche du modèle OSI se situe le segments ?
- Physique
- Liaison de données
- Réseau
- Transport

R: Transport
Un message de taille importante est découpé par la couche transport en segments qui sont transportés par des paquets.

9. Quel protocole peut-on trouver sur la couche 2 ?
- Ethernet
- IP
- TCP
- DCHP

R: Ethernet
Ethernet est le protocole de la couche liaison de données. Ce protocole existe avec plusieurs couches physiques : 100BaseT pour les liaisons 100 Mbit/s sur du cuivre et 10GBaseF pour la Fibre à 10 Gbit/s

10. Quels protocoles peut-on trouver sur la couche 3 ?
- Ethernet
- IP
- TCP
- HTTP

R: IP, Internet Protocol, est le protocole de la couche réseau d'Internet.

11. Quels protocoles peut-on trouver sur la couche 4 ?
- TCP
- HTTP
- Ethernet
- UDP

R: TCP, UDP
UDP, User Datagram Protocol, et TCP, Transmission Control Protocol, sont les deux principales couches de transport sur Internet.


12. Quelles fonctions peut-on attribuer à la liaison de données ?
- La gestion du branchement au support
- Le contrôle CRC des erreurs dans la transmission d'une trame
- L'envoi des bits un à un sur le medium de communication
- La préparation des trames pour la couche 1

R: Le contrôle CRC des erreurs dans la transmission d'un paquet, La préparation des trames pour la couche 1
Sur l'émetteur, la couche liaison de données prépare la trame, notamment en ajoutant un code de détection d'erreur CRC.
Sur le récepteur, la couche liaison de données vérifie le code de détection d'erreur.

13.  Quelles fonctions peut-on attribuer à la couche transport ?
- Le rassemblement des paquets en un seul message
- L'encodage des bits
- L'envoi et la réception d'un accusé de réception
- Le routage des paquets

R: Le rassemblement des paquets en un seul message, L'envoi et la réception d'un accusé de réception
L'encodage des bits est à la charge de la couche physique et le routage des paquets à la charge de la couche réseau.



