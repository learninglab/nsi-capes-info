Associer les applications et les réseaux (il faudrait mélanger évidemment !)

A. Gestion de l'éclairage et de la température d'une maison (domotique)
B. Géolocalisation et Identification de palettes à travers l'Europe
C. Communication entre le contrôleur et les actionneurs d'un exosquelette, pour l'aide à la marche de personne handicapée
D. Communication entre le processeur et la carte vidéo dans un PC
E. Communication entre les boutons et les actionneurs d'un siège de voiture à réglage électrique




1. Zigbee, protocole sans fil très faible consommation, portée de 50 m et débit faible.
2. LoRa, protocole sans fil opéré dans presque toute l'Europe, très longue portée, très faible consommation, très faible débit. LoRa permet la géolocalisation des émetteurs.
3. Ethercat, protocole temps réel s'appuyant sur Ethernet 100 Mbit/s
4. PCIexpress, protocole filaire 32 Gb/s par voie.
5. Lin, protocole filaire très fiable et très bon marché basé sur un liaison série basique. Débit de 10,4 kbit/s

R A1, B2, C3, D4, C5
Cet exercice montre que les usages très différents des réseaux amènent à des solutions adaptées variées.

