\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{mooc-nsi}

\PassOptionsToClass{addpoints,a4paper,11pt}{exam} % format a4paper par dÃ©faut
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{exam}}
\ProcessOptions

\LoadClass[11pt]{exam}
\RequirePackage[english,french]{babel}
\RequirePackage{graphicx}
\RequirePackage{textcomp}
\RequirePackage{lmodern} % affichage plus joli
\RequirePackage[T1]{fontenc} % le rendu des underscores semble meilleur
\RequirePackage[utf8]{inputenc}
\RequirePackage[small,hang]{caption2}
\RequirePackage{calc}
\RequirePackage{vmargin}
\RequirePackage{url}
\RequirePackage{hyperref}
\RequirePackage{moreverb}



%%%% debut macro %%%%
\newenvironment{changemargin}[2]{\begin{list}{}{%
\setlength{\topsep}{0pt}%
\setlength{\leftmargin}{0pt}%
\setlength{\rightmargin}{0pt}%
\setlength{\listparindent}{\parindent}%
\setlength{\itemindent}{\parindent}%
\setlength{\parsep}{0pt plus 1pt}%
\addtolength{\leftmargin}{#1}%
\addtolength{\rightmargin}{#2}%
}\item }{\end{list}}

\newcounter{exo}[subsection]
%\newenvironment{exo}{\refstepcounter{exo}\vspace{0.5cm}{\noindent \bfseries Exercice \theexo\ :}}{\par\vspace{0.5cm}}
\newenvironment{exo}{\refstepcounter{exo}\vspace{0.5cm}{\noindent \bfseries \large Exercice \theexo\
}}{\par\vspace{0.5cm}}
\newenvironment{question}{\refstepcounter{exo}\vspace{0.5cm}{\noindent \bfseries \large Question \thesubsection.\theexo\
}}{\par\vspace{0.5cm}}
\newenvironment{rep}{\refstepcounter{exo}\vspace{0.5cm}{\noindent \bfseries \large Réponse \theexo\
}}{\par\vspace{0.5cm}}
%\newenvironment{exo}{\refstepcounter{exo}\vspace{0.5cm}{\noindent \bfseries \Large Exercice \theexo\}}{\par\vspace{0.5cm}}
%%%% fin macro %%%%








  
