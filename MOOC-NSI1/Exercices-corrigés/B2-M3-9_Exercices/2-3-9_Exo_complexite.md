# Exercices  - Calcul de complexité

**Note :** Pour répondre aux questions, nous vous conseillons d'écrire chaque code et de l'annoter en veillant bien à détailler vos réponses et en justifiant la complexité de chaque partie du code.

## Exercices 1

Indiquez la complexité maximale des codes suivants.

On suppose n entier positif,  $t$  $a$ et $b$ des listes d'entiers (de petites valeurs).

### Possibilités :

- $O(1)$
- $O(n)$
- $O(n^2)$
- $O(n^3)$
- $O(log(n))$
- $O(n \, log(n))$
- $O(2^n)$
- aucune de ces complexités

### Enoncés :

-  ```python
   def sequence(n):
      n = n + 1
      n = n + 4
      n = n * 4
      n = n ** 10
      n = n // 2
      return n
   ```
   **Réponse :** $O(1)$

-  ```python
   def boucle(n):
      n = n + 1
      i = 0
      while i < n:
         i = i + 1
      n = n * n                              
      return n
   ```
   **Réponse :** $O(n)$
   
-  ```python
   def boucle(n):
      n = n + 1
      i = 1
      while i < n:
         i = 2 * i
      n = n * n                              
      return n
   ```
   **Réponse :** $O(log(n))$
 
 - ```python
   def b(t):
     n = len(t)
     for j in range(4):
       for i in range(n):
         t[i] *= t[i]
   ```
   **Réponse :** $O(n)$


- ```python
   def h(t):
     n = len(t)
     for i in range(n):
       t[i] = 0
       j = n
       while j > 1:
         t[i] += i*j
         j //= 2
   ```
   **Réponse :** $O(n \, log(n))$

- ```python
   def nesting(n):
      sum = 0
      i = 0
      while i < n:
         j = 0
         while j < i:
            sum += 1
            j += 1
         i += 1
      return sum
   ```
   **Réponse :** $O(n^2)$

- ```python
   def recherche(s,x):
      i = 0
      while i < len(s) and s[i] != x:
         i = i+1
      if i == len(s):
         i = -1
      return i
   ```
   **Réponse :** $O(n)$

- ```python
   def tri_selection(s):
      n = len(s)
      for i in range(n-1):
         min = i
         for j in range(i+1,n):
           if  s[j] < s[min]:
              min = j
         s[min],s[i] =  s[i],s[min]
   ```
   **Réponse :** $O(n^2)$

- ```python
   def tri_insertion(s):
      n = len(s)
      for i in range(1,n):
         Save = s[i]
         j = i-1
         while j>=0 and s[j] > Save:
            s[j+1] = s[j]
            j=j-1
         s[j+1] = Save
   ```
   **Réponse :** $O(n^2)$


 
## Exercices 2

Chacune des 2 fonctions `foo1` et `foo2` reçoit deux listes `a` et `b` (que l'on supposer de petites valeurs entières) et teste si `b` est une permutation de `a`.

Donnez la complexité moyenne et maximale en $O()$ du temps d'exécution de chacune des deux fonctions.

### Possibilités (complexité moyenne) :

- $O(1)$
- $O(n)$
- $O(n^2)$
- $O(n^3)$
- $O(log(n))$
- $O(n \, log(n))$
- $O(2^n)$
- aucune de ces complexités

### Possibilités (complexité maximale) :

- $O(1)$
- $O(n)$
- $O(n^2)$
- $O(n^3)$
- $O(log(n))$
- $O(n \, log(n))$
- $O(2^n)$
- aucune de ces complexités

### Enoncés :


- ```python
   def foo1(a,b):                                        
       res = False                                       
       if len(a)==len(b):                                
          dic_a = {}                                     
          dic_b = {}                                     
          for i in a:                     
              dic_a[i]=dic_a.get(i,0)+1   
          for i in b:                    
              dic_b[i]=dic_b.get(i,0)+1  
         res = dic_a == dic_b
       return res                                      
   ```
   **Réponse :** : complexité moyenne : $O(n)$  maximale : $O(n^2)$

- ```python
   def foo2(a,b):                                      
       res = False                                     
       if len(a)==len(b):                     
          res = True                          
          for i in a:                    
              if i in b:                 
                  a.remove(i)            
                  b.remove(i)            
              else:                      
                  res = False            
       return res                        
   ```
   **Réponse :** foo2(a,b): complexité moyenne : $O(n^2)$  maximale : $O(n^2)$




