# Exercice UpyLaB de codage en Python

Pour chaque exercice "UpyLab", nous vous demandons d'écrire du code Python qui résout un problème donné.

## Enoncé

Ecrivez une fonction `changement_de_base(rep_a, a, b)`  qui renvoie la représentation en base `b` d'une valeur entière positive ou nulle représentee par rep_a en base `a`.

### Paramètres:

- `rep_a` (`str`) représentation en base `a` de la valeur
- `a`: (`int`) base de `rep_a` avec `1 < a < 37`
- `b`: (`int`) base de la représentation retournée

Renvoie la représentation en base `b` de la valeur représentée par `rep_a` en base `a`

Les chiffres de la représentation jusqu'en base 36 sont donnés par 

`'0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'`

Par exemple `'H'` représente la valeur 17 dans une base au moins égale à 18.

### Conseils:


- Veillez à découper votre code.

- La traduction de la représentation en valeur et vice-versa peut être une découpe pertinente

